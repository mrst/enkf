classdef ModelClass_V2
   properties
      model
      parameters
      runModel
      pE
   end
   methods
      function obj = ModelClass_V2(model,...
                                parameters,...
                                runModel_fun,...
                                varargin)
                                
        opt = struct('Verbose',           mrstVerbose(),...
                     'TimeVar',   'dt');

        opt = merge_options(opt, varargin{:});                                
                            
         if nargin >= 4
            obj.model           = model;
            obj.parameters      = parameters;                                                                                          
                                             
             switch opt.TimeVar
                case 'time'
                     obj.runModel        =@(p2,t) runModel_fun(p2,...
                                                     obj.parameters,...
                                                     obj.model,...
                                                     t); 
                case 'dt'

                     obj.runModel        =@(p2,t) runModel_fun(p2,...
                                                     obj.parameters,...
                                                     obj.model,...
                                                     diff([0; reshape(t,length(t),1)]));  
                otherwise
                   warning('Parameter TimeVar is not implemented')
            end

         end
      end          
   end
end