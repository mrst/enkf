
 if nmembers > 0
     for j=1:nmembers
         
         A(:,j) = [pE{j}'; statesE_new{j}(end)];
         % Assert whether there exists fluidE  
     end
 end 
 nstat = [numel(pE{j}(:)),numel(statesE_new{j}(end))];  

  
 
 if inflation > 0
     B = randn(nb,nmembers);
     for i = 1:nb
         tmp = B(i,:);
         tmp = tmp - mean(tmp); % remove mean
         tmp = tmp ./ std(tmp); % normalize std
         B(i,:) = tmp;
     end
     A = [A; B];
 end
 A0 = A;