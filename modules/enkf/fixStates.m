function [U] = fixStates(U,nstat)
%----------------------------------------------------------------------------------
% SYNOPSIS:
%   [U] = fixStates(U,nstat)
%
% DESCRIPTION:
%   Ensure physical values for updates state variables
%
% PARAMETERS:
%   U           -   ensemble of model states
%   nstat       -   array containing number of elements for each state
%                   variables
%
% RETURNS:
%   U           -   physically valid state variable values
%
%
%{
  Copyright 2008 - 2017, TNO.

  This file is part of the EnKF module for MRST. 

  The EnKF module is free software. You can redistribute it and/or modify it under 
  the terms of the GNU General Public License as published by the Free Software 
  Foundation, either version 3 of the License, or (at your option) any later version.
 
  The EnKF module is distributed in the hope that it will be useful, but WITHOUT 
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along with this 
  code.  If not, see <http://www.gnu.org/licenses/>.
%}
%
% The code has been tested with Matlab version R2014b.
%
% Please acknowledge TNO in any reports or publications that use the EnKF module. 
% TNO welcomes any feedback, improvements and extensions to the code.
%
% Written by Olwijn Leeuwenburgh, TNO.
%----------------------------------------------------------------------------------

% extract updated ensemble states
Trams = U(1:nstat(1),:);
PoreV = U(sum(nstat(1:1))+1:sum(nstat(1:2)),:);
%prfe = U(sum(nstat(1:2))+1:sum(nstat(1:3)),:);
%sate = U(sum(nstat(1:3))+1:sum(nstat(1:4)),:);
if length(nstat) > 4
    stre = U(sum(nstat(1:4))+1:sum(nstat(1:5)),:);
else
    stre = [];
end

% porosity
PoreV(PoreV<=0) = 1.0205e+02;
Trams(Trams<=0) = 0.00027*1.0e-08;
% it=find(pore<0.02); if ~isempty(it), pore(it)=0.02+(0.02-pore(it)).*rand(length(it),1); end
% it=find(pore>0.40); if ~isempty(it), pore(it)=0.40-(pore(it)-0.40).*rand(length(it),1); end

% saturation
% sate(sate<0.0) = 0.0;
% sate(sate>1.0) = 1.0;

% reconstruct fixed ensemble state matrix
U(1:nstat(1),:)                        = Trams;
U(sum(nstat(1:1))+1:sum(nstat(1:2)),:) = PoreV;
%U(sum(nstat(1:2))+1:sum(nstat(1:3)),:) = prfe;
%U(sum(nstat(1:3))+1:sum(nstat(1:4)),:) = sate;
%if length(nstat) > 4
%    U(sum(nstat(1:4))+1:sum(nstat(1:5)),:) = stre;
%end
