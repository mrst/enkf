function [c0] = gaspari (z,c)
%----------------------------------------------------------------------------------
% SYNOPSIS:
%   [c0] = gaspari (z,c)
%
% DESCRIPTION:
%   Evaluate the Gaspari-Cohn correlation function with local support
%
% PARAMETERS:
%   z           -   distance
%   c           -   correlation scale
%
% RETURNS:
%   c0          -   localization factor
%
%
%{
  Copyright 2008 - 2017, TNO.

  This file is part of the EnKF module for MRST. 

  The EnKF module is free software. You can redistribute it and/or modify it under 
  the terms of the GNU General Public License as published by the Free Software 
  Foundation, either version 3 of the License, or (at your option) any later version.
 
  The EnKF module is distributed in the hope that it will be useful, but WITHOUT 
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along with this 
  code.  If not, see <http://www.gnu.org/licenses/>.
%}
%
% The code has been tested with Matlab version R2014b.
%
% Please acknowledge TNO in any reports or publications that use the EnKF module. 
% TNO welcomes any feedback, improvements and extensions to the code.
%
% Written by Olwijn Leeuwenburgh, TNO.
%---------------------------------------------------------------------------------- 
if z < c
  c0=-0.25*(z/c)^5+0.5*(z/c)^4+0.625*(z/c)^3-(5.0/3.0)*(z/c)^2+1;
elseif z < 2*c
  c0=(1.0/12.0)*(z/c)^5-0.5*(z/c)^4+0.625*(z/c)^3+(5.0/3.0)*(z/c)^2-5*(z/c)+4-(2.0/3.0)*(c/z);
else
  c0=0;
end

return