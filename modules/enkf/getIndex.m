function [i, j, k] = getIndex(idx, ni, nj)
%----------------------------------------------------------------------------------
% SYNOPSIS:
%   [i, j, k] = getIndex(idx, ni, nj)
%
% DESCRIPTION:
%   Find the i, j, k indices in a ni x nj x nk grid
%
% PARAMETERS:
%   idx         -   index from array of length (ni x nj x nk)
%
% RETURNS:
%   i, j, k     -   i, j, and k indices in grid with dimenions ni x nj x nk 
%
%
%{
  Copyright 2008 - 2017, TNO.

  This file is part of the EnKF module for MRST. 

  The EnKF module is free software. You can redistribute it and/or modify it under 
  the terms of the GNU General Public License as published by the Free Software 
  Foundation, either version 3 of the License, or (at your option) any later version.
 
  The EnKF module is distributed in the hope that it will be useful, but WITHOUT 
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along with this 
  code.  If not, see <http://www.gnu.org/licenses/>.
%}
%
% The code has been tested with Matlab version R2014b.
%
% Please acknowledge TNO in any reports or publications that use the EnKF module. 
% TNO welcomes any feedback, improvements and extensions to the code.
%
% Written by Olwijn Leeuwenburgh, TNO.
%---------------------------------------------------------------------------------- 
k = ceil(double(idx)/(ni*nj));
j = ceil((double(idx)-(k-1)*(ni*nj))/double(ni));           
i = ceil((double(idx)-(k-1)*(ni*nj)-(j-1)*ni));