%----------------------------------------------------------------------------------
% SYNOPSIS:
%
% DESCRIPTION:
%   Initialization of models before forward simulation.
%
% PARAMETERS:
%
% RETURNS:
%   state0, state0E     -   MRST initial states for truth and ensemble
%   model, modelE       -   MRST initial reservoir model
%   dynamic state and structure arrays for the truth and for all ensemble models
%
%{
  Copyright TNO, Applied Geosciences.

  This file is part of the EnKF module for MRST. 

  The EnKF module is free software. You can redistribute it and/or modify it under 
  the terms of the GNU General Public License as published by the Free Software 
  Foundation, either version 3 of the License, or (at your option) any later version.
 
  The EnKF module is distributed in the hope that it will be useful, but WITHOUT 
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along with this 
  code.  If not, see <http://www.gnu.org/licenses/>.
%}
%
% The code has been tested with Matlab version R2014b.
%
% Please acknowledge TNO in any reports or publications that use the EnKF module. 
% TNO welcomes any feedback, improvements and extensions to the code.
%
% Written by Olwijn Leeuwenburgh, TNO.
%----------------------------------------------------------------------------------
try
    require ad-props ad-blackoil ad-core
catch %#ok<CTCH>
    mrstModule add ad-props ad-blackoil ad-core
end



% Structural updation
if t == 0 && (iter > 1 || iu > 1)
  if length(nstat) > 5      % 4 is transfered into 5 because we have additional fluid parameters
    load(EnkfFile,'U');
    G = U(sum(nstat(1:4))+1:sum(nstat),:);
    clear U
  end
end

% Set up matlab structures for truth (l=1) and ensemble members (l=2)
for l = 1 : 2

 if l == 1
   ne = 0;
 else
   ne = nmembers;
 end

 for j = min(1,ne) : ne
 
   % assign rock properties
   if j > 0
     if iter == 1 && iu == 1
         
       %Distribute an emsemble of parameters in K and phi to start  
       
       rng('shuffle')
       a = (rand(1)-0.5)*0.3  +2.5;
       rng('shuffle')
       b = (rand(1)-0.5)*0.3  +5;
       rng('shuffle')
       c = (rand(1)-0.5)*0.5  +7.5;
       rng('shuffle')
       d = (rand(1)-0.5)*0.001 +0.01;
       pE{j} = [a b c d];
           
     else
       load(EnkfFile,'U');
       pE{j} = U(:,j)';       
       clear U
     end
   end
 
 
   

 
 end

end

clear ne
       