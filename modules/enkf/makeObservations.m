function [Y, R, D, loc, io] = makeObservations(wellData,wellDataE,timeSteps,...
                              obsTimes,obstype,transform,Sigma,alpha,oneobs,...
                              sat,sate,tds,imap,dims,por,pore)
%----------------------------------------------------------------------------------
% SYNOPSIS:
%   [Y, R, D, loc, io] = makeObservations(wellData,wellDataE,timeSteps,...
%                        obsTimes,obstype,transform,Sigma,alpha,oneobs) 
%   [Y, R, D, loc, io] = makeObservations(wellData,wellDataE,timeSteps,...
%                        obsTimes,obstype,transform,Sigma,alpha,oneobs,...
%                        sat,sate,tds,imap,dims,por,pore)
%
% DESCRIPTION:
%   Generate arrays with actual and simulated observations.
%
% PARAMETERS:
%   wellData    -   true production history
%   wellDataE   -   ensemble simulated production history
%   timeSteps   -   time steps associated with production history
%   obsTimes    -   times from which observations are to be collected
%   obstype     -   measurement type
%   transform   -   desired transformation for each measurement type
%   Sigma       -   measurement error standard deviations
%   alpha       -   error variance multiplier
%   oneobs      -   well or grid cell index (for testing purposes)
%   
%   sat         -   true saturation field
%   sate        -   ensemble of simulated saturation fields
%   tds         -   seismic survey times
%   imap        -   indices of grid cells (used when localizing sat data)
%   dims        -   model grid dimensions
%   por         -   true porosity field
%   pore        -   ensemble of porosity fields
%
% RETURNS:
%   Y           -   ensemble of perturbed observations
%   R           -   array with measurement error variances
%   D           -   ensemble of simulated observations
%   loc         -   array with grid indices for each measurement
%   io          -   indices of used observations
%
%
%{
  Copyright 2008 - 2017, TNO.

  This file is part of the EnKF module for MRST. 

  The EnKF module is free software. You can redistribute it and/or modify it under 
  the terms of the GNU General Public License as published by the Free Software 
  Foundation, either version 3 of the License, or (at your option) any later version.
 
  The EnKF module is distributed in the hope that it will be useful, but WITHOUT 
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along with this 
  code.  If not, see <http://www.gnu.org/licenses/>.
%}
%
% The code has been tested with Matlab version R2014b.
%
% Please acknowledge TNO in any reports or publications that use the EnKF module. 
% TNO welcomes any feedback, improvements and extensions to the code.
%
% Written by Olwijn Leeuwenburgh, TNO.
%---------------------------------------------------------------------------------- 
                            
nmembers = size(wellDataE(1).flx,2);

ntimes   = length(timeSteps);
ntimeso  = length(obsTimes);
[~, ~, iobs] = intersect(obsTimes, timeSteps); clear *dummy

if ~exist('tds','var'), tds = []; end

Y = []; R = []; D = []; loc = []; io =[];

if ~isempty(iobs)

  %------------------------------------------------------------------------------
  % well observations (rates and/or pressures)
  %------------------------------------------------------------------------------
  match = 0;
  for io = 1 : length(obstype)
    if ~isempty(find(strncmp(obstype{io},{'flx' 'bhp' 'wct'},3),1)), match=match+1; end
  end
  if match > 0 && timeSteps(end) > 0
      
    nwells = length(wellData); wells = 1 : nwells;
    if ~isempty(oneobs)
      wells = oneobs;
    end

    flx = nan*ones(nwells, ntimes);
    bhp = nan*ones(nwells, ntimes);
    wct = nan*ones(nwells, ntimes);
    for i = wells
      flx(i,:) = wellData(i).flx;
      bhp(i,:) = wellData(i).bhp;
      wct(i,:) = wellData(i).ffl;
    end
    flx = flx(:,iobs);
    bhp = bhp(:,iobs);
    wct = wct(:,iobs);

    flxe = nan*ones(nwells, ntimes, nmembers);
    bhpe = nan*ones(nwells, ntimes, nmembers);
    wcte = nan*ones(nwells, ntimes, nmembers);
    if nmembers > 0
        for i = wells
            flxe(i,:,:) = wellDataE(i).flx; % ntimes x nmembers
            bhpe(i,:,:) = wellDataE(i).bhp;
            wcte(i,:,:) = wellDataE(i).ffl;
        end
    end
    flxe = flxe(:,iobs,:);
    bhpe = bhpe(:,iobs,:);
    wcte = wcte(:,iobs,:); 
               
    % well locations (currently based on first perforated layer only)
    wloc = nan*ones(size(flx));
    for i = wells
        wloc(i,:) = wellData(i).loc(1);
    end

    wloc = reshape(wloc, nwells * ntimeso, 1);
    flx  = reshape(flx,  nwells * ntimeso, 1);
    bhp  = reshape(bhp,  nwells * ntimeso, 1);
    wct  = reshape(wct,  nwells * ntimeso, 1);
    flxe = reshape(flxe, nwells * ntimeso, nmembers);
    bhpe = reshape(bhpe, nwells * ntimeso, nmembers);
    wcte = reshape(wcte, nwells * ntimeso, nmembers);
    
  end

  %------------------------------------------------------------------------------
  % construct measurement vector
  %------------------------------------------------------------------------------      
  loc = []; obs = []; sig = []; scl = [];
  for i = 1:length(obstype)
    if ~isempty(find(strncmp(obstype{i},{'sat'},3),1))
        if ~isempty(intersect(obsTimes,convertTo(tds,day)))
            dummy = sat;
            if ~isempty(oneobs)
                dummy = sat(oneobs);
                loc = [loc; -1*imap(oneobs)];
            else
                loc = [loc; -1*imap];
            end
        end
    else
        dummy = eval(obstype{i});
        if ~isempty(find(strncmp(obstype{io},{'flx' 'bhp' 'wct'},3),1))
            loc = [loc; wloc];
        end
        if ~isempty(find(strncmp(obstype{io},{'dst'},3),1))
            loc = [loc; dloc];
        end
    end
    if ~isempty(transform) && length(transform) >= i
        dummy = applyTransform(dummy,size(dummy,1),transform(i));
    end
    scl = [scl; max(abs(dummy)) * ones(size(dummy))];
    obs = [obs; dummy];
    sig = [sig; Sigma(i) * ones(size(dummy))];
    clear dummy
  end

  if nmembers > 0
    obse = [];    
    for i = 1:length(obstype)
      if ~isempty(find(strncmp(obstype{i},{'sat'},3),1))
        if ~isempty(intersect(obsTimes,convertTo(tds,day)))
            dummy = sate;
            if ~isempty(oneobs)
                dummy = sate(oneobs,:);
            end
        end
      else  
        dummy = eval([obstype{i} 'e']);
      end
      if ~isempty(transform) && length(transform) >= i
        if ~exist('dims','var') dims = []; end
        dummy = applyTransform(dummy,size(dummy,1),transform(i),dims);
      end
      obse = [obse; dummy]; 
      clear dummy
    end
  end

  clear bhp* flx* wct* sat* wloc dloc

  %------------------------------------------------------------------------------
  % identify obsolete observations
  %------------------------------------------------------------------------------
  if nmembers > 0
    remove1 = find(max(abs(repmat(obs,1,nmembers) - obse),[],2) < eps);
    remove2 = find(std(obse,[],2) < eps);
    remove3 = find(isnan(obs));
    remove  = union(remove1,remove2); remove = union(remove,remove3);
    io      = setdiff(1:length(obs),remove);
  end

  %------------------------------------------------------------------------------               
  % make ensemble of observations by random perturbation
  %------------------------------------------------------------------------------
  if ~isempty(obs)
    [Y, R] = perturbObservations(obs, sqrt(alpha) * sig, nmembers);
  end

  %------------------------------------------------------------------------------
  % ensemble of simulated observations
  %------------------------------------------------------------------------------
  if nmembers > 0
    D = obse;
  else
    D = [];
  end

  % scaling
  Y = Y ./ repmat(scl,1,nmembers);
  D = D ./ repmat(scl,1,nmembers);
  R = R ./ (scl.^2);

  clear obs obse 

end