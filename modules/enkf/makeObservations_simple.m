function [Y, R, D, loc, io] = makeObservations_simple(model_obs,statesE_new,timeSteps,...
                              obsTimes,obstype,transform,Sigma,alpha,oneobs,wellobs_indx)
                          
%transform   =transformobs;
%alpha =ai;

%----------------------------------------------------------------------------------
% SYNOPSIS:
%   [Y, R, D, loc, io] = makeObservations(wellData,wellDataE,timeSteps,...
%                        obsTimes,obstype,transform,Sigma,alpha,oneobs) 
%   [Y, R, D, loc, io] = makeObservations(wellData,wellDataE,timeSteps,...
%                        obsTimes,obstype,transform,Sigma,alpha,oneobs,...
%                        sat,sate,tds,imap,dims,por,pore)
%
% DESCRIPTION:
%   Generate arrays with actual and simulated observations.
%
% PARAMETERS:
%   wellData    -   true production history
%   wellDataE   -   ensemble simulated production history
%   timeSteps   -   time steps associated with production history
%   obsTimes    -   times from which observations are to be collected
%   obstype     -   measurement type
%   transform   -   desired transformation for each measurement type
%   Sigma       -   measurement error standard deviations
%   alpha       -   error variance multiplier
%   oneobs      -   well or grid cell index (for testing purposes)
%   
%   sat         -   true saturation field
%   sate        -   ensemble of simulated saturation fields
%   tds         -   seismic survey times
%   imap        -   indices of grid cells (used when localizing sat data)
%   dims        -   model grid dimensions
%   por         -   true porosity field
%   pore        -   ensemble of porosity fields
%
% RETURNS:
%   Y           -   ensemble of perturbed observations
%   R           -   array with measurement error variances
%   D           -   ensemble of simulated observations
%   loc         -   array with grid indices for each measurement
%   io          -   indices of used observations
%
%
%{
  Copyright 2008 - 2017, TNO.

  This file is part of the EnKF module for MRST. 

  The EnKF module is free software. You can redistribute it and/or modify it under 
  the terms of the GNU General Public License as published by the Free Software 
  Foundation, either version 3 of the License, or (at your option) any later version.
 
  The EnKF module is distributed in the hope that it will be useful, but WITHOUT 
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along with this 
  code.  If not, see <http://www.gnu.org/licenses/>.
%}
%
% The code has been tested with Matlab version R2014b.
%
% Please acknowledge TNO in any reports or publications that use the EnKF module. 
% TNO welcomes any feedback, improvements and extensions to the code.
%
% Written by Olwijn Leeuwenburgh, TNO.
%---------------------------------------------------------------------------------- 
                            
nmembers = size(statesE_new,2);

ntimes   = length(timeSteps);
ntimeso  = length(obsTimes);
[~, ~, iobs] = intersect(obsTimes, timeSteps); clear *dummy

if ~exist('tds','var'), tds = []; end

Y = []; R = []; D = []; loc = []; io =[];

if ~isempty(iobs)

  n_var = size(model_obs,2);
  
  obs =  zeros(n_var*size(iobs,1),1); %This is the ammount of observations points
  obse = zeros(n_var*size(iobs,1),numel(statesE_new) );
  sig  = [];
  scl  = [];
  
  reel_2 = 0;
  for i_obs = 1:length(obstype)
      
  % TODO: This part has to be replace by a Observation operator
  reel =1;
   
  dummy = [];
  n_var = numel(wellobs_indx{i_obs}{1});
      for k =1:size(obsTimes,2)
       
      obs((1:n_var)+(reel-1)*(n_var) + reel_2,1) = model_obs(iobs(reel),wellobs_indx{i_obs}{1})'; % TODO obsTimes is a time variable and 
      dummy =  [dummy; obs((1:n_var)+(reel-1)*(n_var) + reel_2,1)];
     % obs(k,1)= model_obs(obsTimes(k)); % TODO obsTimes is a time variable and 
      % we acctually need the index of the corresponding observation at this
      % time. Remove "obsTimes(k)" and Include the index of the conrresponding observation
      % time 
        for i = 1:numel(statesE_new)  
            obse((1:n_var)+(reel-1)*(n_var) + reel_2,i) = statesE_new{i}.data(iobs(reel),wellobs_indx{i_obs}{1})'; %TODO: This transpose will give problems in the future, try with reshape
%            obse((1:n_var)+(reel-1)*(n_var) + reel_2,i) = statesE_new{i}(iobs(reel),wellobs_indx{i_obs}{1})'; %TODO: This transpose will give problems in the future, try with reshape
        end
        reel = reel + 1;        
      end
      
      reel_2 = reel_2 + length(dummy);
  sig = [sig; 0*dummy +  Sigma(i_obs)];
  scl = [scl; 0*dummy +  max(abs(dummy))];
  end
  %------------------------------------------------------------------------------
  % identify obsolete observations
  %------------------------------------------------------------------------------
%TODO: Re-implement the removing of obsolete observations
    if nmembers > 0
    remove1 = find(max(abs(repmat(obs,1,nmembers) - obse),[],2) < eps);
    remove2 = find(std(obse,[],2) < eps);
    remove3 = find(isnan(obs));
    remove  = union(remove1,remove2); remove = union(remove,remove3);
    io      = setdiff(1:length(obs),remove);
  end

  %------------------------------------------------------------------------------               
  % make ensemble of observations by random perturbation
  %------------------------------------------------------------------------------
  if ~isempty(obs)
    [Y, R] = perturbObservations(obs, sqrt(alpha) * sig, nmembers);
  end

  %------------------------------------------------------------------------------
  % ensemble of simulated observations
  %------------------------------------------------------------------------------
  if nmembers > 0
    D = obse;
  else
    D = [];
  end

  % scaling
  Y = Y ./ repmat(scl,1,nmembers);
  D = D ./ repmat(scl,1,nmembers);
  R = R ./ (scl.^2);

  clear obs obse 

end