function  workdir = mrstEnKF(input_file,varargin)

%----------------------------------------------------------------------------------
% SYNOPSIS:
%   workdir = mrstEnKF(input_file,varargin)
%
% PARAMETERS:
%   input_file - "m-file" with input parameters for mrstEnKF.
%
% OPTIONAL PARAMETERS
%
%    'workdir' - path to directory in which to save all output variables. 
%                 Default is <mrstOutputDirectory>/ENKF/inputfile/    
% 
% RETURNS:
%     workdir - path to directory where results are saved. This can be used as
%                 input to plotProduction().  
%    
% DESCRIPTION:
%   Main program of the Ensemble Kalman Filter module for compressible models
%    in the SINTEF reservoir simulator MRST.
%
%{
  Copyright TNO, Applied Geosciences.

  This file is part of the EnKF module for MRST. 

  The EnKF module is free software. You can redistribute it and/or modify it under 
  the terms of the GNU General Public License as published by the Free Software 
  Foundation, either version 3 of the License, or (at your option) any later version.
 
  The EnKF module is distributed in the hope that it will be useful, but WITHOUT 
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along with this 
  code.  If not, see <http://www.gnu.org/licenses/>.
%}
%
% The code has been tested with Matlab version R2014b.
%
% Please acknowledge TNO in any reports or publications that use the EnKF module. 
% TNO welcomes any feedback, improvements and extensions to the code.
%
% Written by Olwijn Leeuwenburgh, TNO.
%----------------------------------------------------------------------------------

 
%----------------------------------------------------------------------------------
% input section
%----------------------------------------------------------------------------------
close all

[~ ,name, ~] = fileparts(input_file);
workdir = [mrstOutputDirectory filesep 'ENKF' filesep   convertStringsToChars(name) ];   

opt = struct('workdir',workdir);
opt = merge_options(opt, varargin{:});

workdir =  opt.workdir;


run(input_file)

% Change dt into vector
if size(dt,2) == 1 
    dt = dt*ones(tsim/dt,1);
end    
%----------------------------------------------------------------------------------
% loop over experiments
%----------------------------------------------------------------------------------
 for ir = 1 : nrepeats + 1

 rng(ir);
 
 %---------------------------------------------------------------------------------
 % preparation
 %---------------------------------------------------------------------------------
 if nrepeats > 0
    workdir = [Settings.workdir '_r' num2str(ir)];
 end
 if exist(workdir,'dir')
   [~, ~, ~] = rmdir(workdir,'s');
 end
 [status, message, messageid] = mkdir(workdir);
 %cd(workdir);

 %---------------------------------------------------------------------------------
 % loop over update times
 %---------------------------------------------------------------------------------
 t = 0; time = convertTo(t,day);
 for iu = 1 : length(tda)
   tu = tda(iu);
   
   % select observation times for the current update step
   if iu > 1
      obsTimes = intersect(tdo(tdo > tda(iu-1)),tdo(tdo <= tda(iu)));
   else
      obsTimes = tdo(tdo <= tda(iu));
   end
   obsTimes = convertTo(obsTimes,day);

   %-------------------------------------------------------------------------------
   % loop over iterations
   %-------------------------------------------------------------------------------
   iter = 1; niter = niterations + 1;
   if tu == tsim && tsim > tend
       niter = 1;
   end
   while iter <= niter

     if iter > 1
        % restart from previous update time
        if iu > 1
            t = tda(iu-1);
        end
        % restart from time zero
        if restart == 1
            t = 0;
            clear timeSteps wellData*
        end
     end

     time = convertTo(t,day);

     %-----------------------------------------------------------------------------
     % read current states from EnKF file or rebuild grid and run model from time 0
     %-----------------------------------------------------------------------------
     if isIncomp ==1
      initializeModel;
     else
      initializeModel_comp;
     end
     

     %-----------------------------------------------------------------------------
     % simulate to next update time
     %-----------------------------------------------------------------------------
     if isIncomp ==1
        simulateModel;
     else
        simulateModel_comp;
     end


     %-----------------------------------------------------------------------------
     % construct ensemble state matrix A
     %-----------------------------------------------------------------------------
     if isIncomp ==1
        constructEnsemble;
     else
        constructEnsemble_comp;
     end

     %-----------------------------------------------------------------------------
     % observations
     %-----------------------------------------------------------------------------
     ai = Alpha(min(numel(Alpha),iter));
     if ~isempty(find(strncmp('sat',obstype,3),1)) || ~isempty(find(strncmp('dst',obstype,3),1))
        por = rock.poro;
        for j = 1 : nmembers
            pore(:,j) = rockE{j}.poro;
        end
        [Y,R,D,loc,io] = makeObservations(wellData,wellDataE,timeSteps,...
                             obsTimes,obstype,transformobs,Sigma,ai,oneobs,...
                             sat,sate,tds,imap,Grid.cartDims,por,pore);
     else
        [Y,R,D,loc,io] = makeObservations(wellData,wellDataE,timeSteps,...
                             obsTimes,obstype,transformobs,Sigma,ai,oneobs); 
     end
     clear sat* ai
     
     %-----------------------------------------------------------------------------
     % remove obsolete observations
     %-----------------------------------------------------------------------------
     if size(Y,1) > 0
        if iter == 1
            ro = setdiff(1:size(Y,1),io); ro0 = ro;
        else
            ro = ro0;
        end
        Y(ro,:) = []; D(ro,:) = []; R(ro) = []; loc(ro) = [];
     end

     nobs = size(Y,1);
     if nobs == 0, iter = niter; end

     %-----------------------------------------------------------------------------
     % evaluate mean normalized squared model-data misfit
     %-----------------------------------------------------------------------------
     mse = evaluateMSE(Y, D, R);
     
     %-----------------------------------------------------------------------------
     % update the models if needed
     %-----------------------------------------------------------------------------
     if nmembers > 0 && iter < niter
         
       %---------------------------------------------------------------------------
       % transformation of states
       %---------------------------------------------------------------------------
       A = applyTransform(A,nstat,transform,Grid.cartDims,workdir);

       EnkfFile =  [workdir filesep 'states_' num2str(time) '_' num2str(iter) '.mat'];
       %---------------------------------------------------------------------------
       % initialization, stopping criteria and step size adjustment for EnRML
       %---------------------------------------------------------------------------
       if strcmp(method,'EnRML')
           prepareEnRML;
       end
       
       %---------------------------------------------------------------------------
       % ensemble update
       %---------------------------------------------------------------------------
       if iter < niter
         updateEnsemble;
       end

       %---------------------------------------------------------------------------
       % inflation
       %---------------------------------------------------------------------------
       if inflation > 0
         inflateEnsemble;
       end

       %---------------------------------------------------------------------------
       % back transformation of states
       %---------------------------------------------------------------------------
       U = applyTransform(U,nstat,itransform,Grid.cartDims,workdir);

       %---------------------------------------------------------------------------
       % fix updated states
       %---------------------------------------------------------------------------
       U = fixStates(U,nstat);

       %---------------------------------------------------------------------------
       % store results from udating
       %---------------------------------------------------------------------------
       if strcmp(method,'EnRML')
          save(EnkfFile,'xt','U','A0','Y0','A1','J','beta');
       else
          save(EnkfFile,'xt','U','A0','mse');
       end
       
     else

       %---------------------------------------------------------------------------
       % store results from simulation with updated models
       %---------------------------------------------------------------------------
       EnkfFile = [workdir filesep 'states_' num2str(time) '_' num2str(iter) '.mat'];
       U = A;
       if isempty(G)
          save(EnkfFile,'xt','U','mse');
       else
          save(EnkfFile,'xt','U','mse','gridE');
       end
        
     end
     clear A A0 U xt

     %-----------------------------------------------------------------------------
     % save experiment input settings and well data
     %-----------------------------------------------------------------------------
     summary = [workdir filesep 'summary_' num2str(time) '_iter' num2str(iter) '.mat'];
     if nmembers > 0
       save(summary,'wellData','wellDataE','timeSteps','Settings','well');
     else
       save(summary,'wellData','timeSteps','Settings','well');
     end
     
     iter = iter+1;
     
   end
 
 end
 

%----------------------------------------------------------------------------------
 end % repeated experiments
%----------------------------------------------------------------------------------
