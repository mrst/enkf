function pE = mrstEnKF2(model,observation,varargin)



% Run a data asimilations process
%
% SYNOPSIS:
%   mrstEnKF2(model,observation,varargin)
% DESCRIPTION:
%
% REQUIRED PARAMETERS:
%
%   'method'         -  Method (EnKF,LCEnKF,EnRML). For ES-MDA, use EnKF 
%                       with niterations > 1
%
%   'scheme'         - The solver may not use timesteps equal to the
%                      update scheme (when method is EnKF)
%                       1: inverse computed using Matlab functions \ and / and full rank R
%                       2: pseudo inverse with full rank R
%                       3: subspace pseudo inverse with low rank Re (default)
%
%   'restart'        - 0: continue simulation after each update
%                      1: restart from time 0 after each update
%
%   'inflation'       - see applyTransform.m for details
%
%   'niterations      - max number of iterations to same update time
%                       0: no update, 1: one update, 2: use same data twice, etc.
%
%   'Alpha'           - data error-variance inflation factors for niterations
%                       e.g. for 4 ES-MDA iterations: alpha = [9.333 7 4 2];
%
%   'nrepeats'        - rumber of repeats of the experiment
%
%   'tolerance'       - tolerance used in pseudo inversion w
%
%   'transformobs'    - transformation parameters for observations  
%
%   'Sigma'           - observation error standard deviations
%                    
%   'range'           - localization range: c(2*range) = 0, where 
%                        for c the Gaspari-Cohn function is used. only
%                        used with LCEnKF
%                                         
%   'tsim'            -  simulation end time
%   'tend'            -  history match end time
%   'dt'              -  simulation time step
%   'dtu'             -  update time interval
%   'dto'             -  well data time interval (multiple of dt)                
%   'dts'             -  seismic data time interval (multiple of dtu)
%                                     % only used when obstype includes 'sat'
%                       
% RETURNS:
%   s         
%
% NOTE:
%  
%
% SEE ALSO:
%   

%{
Copyright 2009-2019 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}




t_obs     = observation.time;
model_obs = observation.data;
%----------------------------------------------------------------------------------
 oneobs         = [];                   % index of single well or grid cell observation
 ts_real        =  1;          % The initilized time step in real reservoir simulation                           
 Re2Data        = true;  % consider the data_driven effects

%----------------------------------------------------------------------------------
% input section
%----------------------------------------------------------------------------------
close all


workdir = [mrstOutputDirectory filesep 'ENKF' filesep  'output'];   

 Settings = struct(  'workdir',      workdir,    ... 
                     'modelname',   'output',  ... %Necesary?
                     'method',      'EnKF',     ...
                     'scheme',      1,     ...
                     'nmembers',    numel(model.pE),...  %Necesary?
                     'restart',     1,    ...
                     'inflation',   0,  ...
                     'transform',  {{0}}, ...
                     'niterations', 1,...
                     'Alpha',       1,      ...
                     'nrepeats',    0,   ...
                     'tolerance',   0.01,  ...
                     'tsim',        [],       ...   %Necesary?
                     'tend',        [],       ...   %Necesary?
                     'dt',          [],         ...
                     'dto',         [],        ...
                     'dts',         [],        ...
                     'dtu',         [],        ...
                     'transformobs',{{0}},...      %Variable related to the scaling transformation of eac parameter               
                     'itransformobs',{{0}},...
                     'nstat'        ,[], ... %This should be defined by the number of parameters
                     'Sigma',       0.01,...
                     'obsnum', [],...
                     'obstype',[],...
                     'wellobs_indx', []);

Settings = merge_options(Settings, varargin{:});

%% Method parameters
 workdir        = Settings.workdir    ;
 method         = Settings.method     ;
 scheme         = Settings.scheme     ;
 nmembers       = Settings.nmembers   ;
 restart        = Settings.restart    ;     

 inflation      = Settings.inflation  ;
 obstype        = Settings.obstype    ;
 wellobs_indx   = Settings.wellobs_indx;
 
 transform      = Settings.transform  ; 
 niterations    = Settings.niterations; 
 Alpha          = Settings.Alpha      ; 
 nrepeats       = Settings.nrepeats   ; 
 tolerance      = Settings.tolerance  ; 

 transformobs   = Settings.transformobs;
 itransformobs  = Settings.itransformobs;
 nstat          = Settings.nstat;

 Sigma          = Settings.Sigma      ; 
 %range          = Settings.range     ; 

 tsim           = Settings.tsim       ; 
 tend           = Settings.tend       ; 
 dt             = Settings.dt         ; 
 dtu            = Settings.dtu        ; 
 dto            = Settings.dto        ; 
 dts            = Settings.dts        ; 

 
% The cumulative update time for each time interval
% Consider the effects of uneven time step
% (This should be defined by users again according to specific schedule.)                                                      
               
 dt = [dt,diff(dt:dt:tsim)];

 dtu = [dtu,diff(dtu:dtu:tend)];
 
  % New time steps for different time scales
 dto = [dto,diff(dto:dto:tend)];
 
 dtcum = cumsum(dt);           
 dtucum = cumsum(dtu);
 dtocum = cumsum(dto);
 No_dt = length(dt);
 tda = union(dtucum,tend);         % The vector of updating timestep
 tdo = union(dtocum,tend);         % The vector of observation timestep  

 
 % The cumulative simulation time step and updating time step
 tds = union(dts:dts:tend,tend);
 tda = union(tda,tsim);
 
%TODO Check this conditional
 if isempty(intersect(obstype,{'flx' 'bhp' 'wct'}))
     dto = dts;
 end

 itransform = transform;
 for i = 1 : length(transform)
     itransform{i}(1) = -1 * transform{i}(1);
 end

 obsnum = zeros(1,numel(obstype)); % 1: flx,2: bhp,3: wct,4: sat, 5: dst
 for i = 1:numel(obstype)
     obsnum(i) = find(strncmp(obstype{i},{'flx' 'bhp' 'wct' 'sat' 'dst'},3),1);
 end
 
 % initialisation of geo-model/structural parameters
 G = []; 
 
 
 
 if niterations == 0
     restart = 0;
 end
 
 % rows in dummy matrix B for adaptive inflation
 nb = 10;
 
 % iteration and step size parameters
 J = [];
 Jmin = 1.e+32;     
 beta = 1;         
 epsilon = [1.e-6 1.e-6];
   
 
%%----------------------------------------------------------------------------------

% Change dt into vector
if size(dt,2) == 1 
    dt = dt*ones(tsim/dt,1);
end    
%----------------------------------------------------------------------------------
% loop over experiments
%----------------------------------------------------------------------------------
 for ir = 1 : nrepeats + 1

 rng(ir);
 
 %---------------------------------------------------------------------------------
 % preparation
 %---------------------------------------------------------------------------------
 if nrepeats > 0
    workdir = [Settings.workdir '_r' num2str(ir)];
 end
 if exist(workdir,'dir')
   [~, ~, ~] = rmdir(workdir,'s');
 end
 [status, message, messageid] = mkdir(workdir);
 %cd(workdir);

 %---------------------------------------------------------------------------------
 % loop over update times
 %---------------------------------------------------------------------------------
 t = 0; time = convertTo(t,day);
 for iu = 1 : length(tda)
   tu = tda(iu);
   
   % select observation times for the current update step
   if iu > 1
      obsTimes = intersect(tdo(tdo > tda(iu-1)),tdo(tdo <= tda(iu)));
   else
      obsTimes = tdo(tdo <= tda(iu));
   end

   %-------------------------------------------------------------------------------
   % loop over iterations
   %-------------------------------------------------------------------------------
   iter = 1; niter = niterations + 1;
   if tu == tsim && tsim > tend
       niter = 1;
   end
   while iter <= niter

     if iter > 1
        % restart from previous update time
        if iu > 1
            t = tda(iu-1);
        end
        % restart from time zero
        if restart == 1
            t = 0;
            clear timeSteps wellData*
        end
     end

     time = t; 

     %-----------------------------------------------------------------------------
     %% read current states from EnKF file or rebuild grid and run model from time 0
     %-----------------------------------------------------------------------------
        % Structural updation
        if t == 0 && (iter > 1 || iu > 1)
          if length(nstat) > 5      % 4 is transfered into 5 because we have additional fluid parameters
            load(EnkfFile,'U');
            G = U(sum(nstat(1:4))+1:sum(nstat),:);
            clear U
          end
        end

        % Set up matlab structures for truth (l=1) and ensemble members (l=2)
        for l = 1 : 2

         if l == 1
           ne = 0;
         else
           ne = nmembers;
         end

         for j = min(1,ne) : ne
           % assign rock properties
           if j > 0
             if iter == 1 && iu == 1
              % TODO Generalize a the ensembles of parameters
                pE{j} = model.pE{j};
             else
               load(EnkfFile,'U');
               pE{j} = U(:,j); %TODO: add ' for dd-model egg model
               clear U
             end
           end 
         end

        end

        clear ne

     

     %-----------------------------------------------------------------------------
     %% simulate to next update time
     %-----------------------------------------------------------------------------
        % Store the initial time and prepare to simulate through this.
        t_or = t;

        % Assert whether it uses previous simulation time.   
        if       t == 0 
           ts_real = 0;                          % run from time zero
           istep   = 0;
        elseif   t > 0 
           ts_real  = ts_real_or;                 % run from the last update time 
           istep_or = istep;
        end

        %---------------------------------------------------------------------------
        % Iterate timestep in every updating time
        %--------------------------------------------------------------------------- 
        % istep is the index of vector dt. ts_real and ts_real_or is similar to 
        % istep. They can be used if users want to extract history production 
        % data because ther can store previous updating time step. step represents 
        % every time step in simulation process.
     

        while t < tu       
            ts_real = ts_real + 1;
            % When iter is 1, store ts_real. 
            if  iter == 1  
                 ts_real_or   = ts_real;
            end  
             step = dt(ts_real); if t + dt(ts_real) > tu; step = tu-t; end  
             t = t + step;  time = t;
             
            if ~exist('timeSteps','var')
                 timeSteps = [];
            end            
            timeSteps = [timeSteps; time]; istep = istep + 1;                        
        end

         %---------------------------------------------------------------------------
         % store production data time series (rates in m^3/s, pressures in Pa)
         %---------------------------------------------------------------------------    
         % simulate truth <= REMOVED

         % simulate ensemble
        parfor j = 1 : numel(model.pE)
            statesE_new{j} = model.runModel(pE{j},cumsum(dt(1:istep)));
        end
        %storeProduction_comp;
        %clear rate flow pressure fracflow 

        disp(['finished all simulations. time = ' num2str(time)])   


     %-----------------------------------------------------------------------------
     %% construct ensemble state matrix A
     %-----------------------------------------------------------------------------
         if nmembers > 0
             for j=1:nmembers

                 A(:,j) = reshape(pE{j},numel(pE{j}),1);
                 % Assert whether there exists fluidE  
             end
         end 




         if inflation > 0
             B = randn(nb,nmembers);
             for i = 1:nb
                 tmp = B(i,:);
                 tmp = tmp - mean(tmp); % remove mean
                 tmp = tmp ./ std(tmp); % normalize std
                 B(i,:) = tmp;
             end
             A = [A; B];
         end
         A0 = A;

     %-----------------------------------------------------------------------------
     %% observations
     %-----------------------------------------------------------------------------
     ai = Alpha(min(numel(Alpha),iter));

%     [Y,R,D,loc,io] = makeObservations(wellData,wellDataE,timeSteps,...
%                             obsTimes,obstype,transformobs,Sigma,ai,oneobs); 

   
     %makeObservations_simple
      [Y,R,D,loc,io] = makeObservations_simple(model_obs,statesE_new,timeSteps,...
                              obsTimes,obstype,transformobs,Sigma,ai,oneobs,wellobs_indx);
     %-----------------------------------------------------------------------------
     % remove obsolete observations
     %-----------------------------------------------------------------------------
     
     %TODO : This part of removing obsolete observation has to be re implemented has well
     
     if size(Y,1) > 0
        if iter == 1
            ro = setdiff(1:size(Y,1),io); ro0 = ro;
        else
            ro = ro0;
        end
        Y(ro,:) = []; D(ro,:) = []; R(ro) = [];% loc(ro) = [];
     end

     nobs = size(Y,1);
     if nobs == 0, iter = niter; end

     %-----------------------------------------------------------------------------
     % evaluate mean normalized squared model-data misfit
     %-----------------------------------------------------------------------------
     mse = evaluateMSE(Y, D, R);
     
     %-----------------------------------------------------------------------------
     % update the models if needed
     %-----------------------------------------------------------------------------

               
     if nmembers > 0 && iter < niter
         
       %---------------------------------------------------------------------------
       % transformation of states
       %---------------------------------------------------------------------------
       A = applyTransform(A,nstat,transformobs,0,workdir);  %TODO, 0 value should be remove, no used parameter.

       EnkfFile =  [workdir filesep 'states_' num2str(time) '_' num2str(iter) '.mat'];
       %---------------------------------------------------------------------------
       % initialization, stopping criteria and step size adjustment for EnRML
       %---------------------------------------------------------------------------
       if strcmp(method,'EnRML')
           prepareEnRML;
       end
       
       %---------------------------------------------------------------------------
       % ensemble update
       %---------------------------------------------------------------------------
       if iter < niter
         updateEnsemble;
       end

       %---------------------------------------------------------------------------
       % inflation
       %---------------------------------------------------------------------------
       if inflation > 0
         inflateEnsemble;
       end

       %---------------------------------------------------------------------------
       % back transformation of states
       %---------------------------------------------------------------------------
       U = applyTransform(U,nstat,itransformobs,0,workdir); %TODO : remove 0 value from the parameter function

       %---------------------------------------------------------------------------
       % fix updated states
       %---------------------------------------------------------------------------
       U = fixStates(U,nstat); %TODO this function needs to be be
        % re-implemented

       %---------------------------------------------------------------------------
       % store results from udating
       %---------------------------------------------------------------------------
       if strcmp(method,'EnRML')
          save(EnkfFile,'U','A0','Y0','A1','J','beta');
       else
          save(EnkfFile,'U','A0','mse');
       end
       
     else

       %---------------------------------------------------------------------------
       % store results from simulation with updated models
       %---------------------------------------------------------------------------
       EnkfFile = [workdir filesep 'states_' num2str(time) '_' num2str(iter) '.mat'];
       U = A;
       if isempty(G)
          save(EnkfFile,'U','mse');
       else
          save(EnkfFile,'U','mse','gridE');
       end
        
     end
     clear A A0 U xt

     %-----------------------------------------------------------------------------
     % save experiment input settings and well data
     %-----------------------------------------------------------------------------
     summary = [workdir filesep 'summary_' num2str(time) '_iter' num2str(iter) '.mat'];
     if nmembers > 0
       save(summary,'statesE_new','timeSteps','Settings');
     else
       save(summary,'timeSteps','Settings');
     end
     
     iter = iter+1;
     
   end
 
 end
 

%----------------------------------------------------------------------------------
 end % repeated experiments
%-------------------------------
