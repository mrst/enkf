%----------------------------------------------------------------------------------
% SYNOPSIS:
%
% DESCRIPTION:
%   Routine for plotting time series output from mrstEnKF.
%
%{
  Copyright 2008 - 2017, TNO.

  This file is part of the EnKF module for MRST. 

  The EnKF module is free software. You can redistribute it and/or modify it under 
  the terms of the GNU General Public License as published by the Free Software 
  Foundation, either version 3 of the License, or (at your option) any later version.
 
  The EnKF module is distributed in the hope that it will be useful, but WITHOUT 
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along with this 
  code.  If not, see <http://www.gnu.org/licenses/>.
%}
%
% The code has been tested with Matlab version R2014b.
%
% Please acknowledge TNO in any reports or publications that use the EnKF module. 
% TNO welcomes any feedback, improvements and extensions to the code.
%
% Written by Olwijn Leeuwenburgh, TNO.
%----------------------------------------------------------------------------------

%---------------------------------------------------------------------------------- 
% input
%----------------------------------------------------------------------------------
 workDir        = [ROOTDIR 'TEST/'];
 timeObs        = 1168;
 timeObj        = 2336;
 iter           = 1;

 sigma = 1.0 * unit;    % measurement error standard deviation
 
%---------------------------------------------------------------------------------- 
% load data file
%----------------------------------------------------------------------------------
 summaryFile    = [workDir 'summary_2336_iter1.mat'];
 load(summaryFile,'wellData','wellDataE','timeSteps','settings');
  
 ntimes = length(timeSteps);
 nwells = length(wellData); 
 
 if exist('wellDataE','var')
     ne = size(rateE,2);
 end
 
%---------------------------------------------------------------------------------- 
% well time series
%---------------------------------------------------------------------------------- 
 ql  = nan*ones(ntimes,nwells); qlE  = nan*ones(ntimes,ne,nwells);
 qo  = nan*ones(ntimes,nwells); qoE  = nan*ones(ntimes,ne,nwells);  
 qw  = nan*ones(ntimes,nwells); qwE  = nan*ones(ntimes,ne,nwells);
 bhp = nan* ones(ntimes,nwells); bhpE  = nan*ones(ntimes,ne,nwells);
 wct = nan* ones(ntimes,nwells); wctE  = nan*ones(ntimes,ne,nwells);
 
 for iw = 1 : nwells
     
     ql(:,iw) = wellData(iw).flx;
     wct(:,iw) = wellData(iw).ffl;
     bhp(:,iw) = wellData(iw).bhp;
     qw(:,iw) = ql(:,iw) .* wct(:,iw);
     qo(:,iw) = ql(:,iw) - qw(:,iw);
     
     qlE(:,:,iw) = wellDataE(iw).flx;
     wctE(:,:,iw) = wellDataE(iw).ffl;
     bhpE(:,:,iw) = wellDataE(iw).bhp;
     qwE(:,:,iw) = qlE(:,:,iw) .* wctE(:,:,iw);
     qoE(:,:,iw) = qlE(:,:,iw) - qwE(:,:,iw);
     
 end
 
%---------------------------------------------------------------------------------- 
% objective function
%---------------------------------------------------------------------------------- 




