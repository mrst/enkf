%----------------------------------------------------------------------------------
% SYNOPSIS:
%
% DESCRIPTION:
%   Simulate synthetic truth and model ensemble to the next update time or to the 
%   specified end time and store production time series.
%
% PARAMETERS:
%
% RETURNS:
%   states, statesE         -   MRST states for truth and ensemble
%   wellData, wellDataE     -   production time series
%
%{
  Copyright TNO, Applied Geosciences.

  This file is part of the EnKF module for MRST. 

  The EnKF module is free software. You can redistribute it and/or modify it under 
  the terms of the GNU General Public License as published by the Free Software 
  Foundation, either version 3 of the License, or (at your option) any later version.
 
  The EnKF module is distributed in the hope that it will be useful, but WITHOUT 
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along with this 
  code.  If not, see <http://www.gnu.org/licenses/>.
%}
%
% The code has been tested with Matlab version R2014b.
%
% Please acknowledge TNO in any reports or publications that use the EnKF module. 
% TNO welcomes any feedback, improvements and extensions to the code.
%
% Written by Olwijn Leeuwenburgh, TNO.
%---------------------------------------------------------------------------------- 

% Store the initial time and prepare to simulate through this.
t_or = t;

% Assert whether it uses previous simulation time.   
if       t == 0 
   ts_real = 0;                          % run from time zero
   istep   = 0;
elseif   t > 0 
   ts_real  = ts_real_or;                 % run from the last update time 
   istep_or = istep;
end

%---------------------------------------------------------------------------
% Iterate timestep in every updating time
%--------------------------------------------------------------------------- 
% istep is the index of vector dt. ts_real and ts_real_or is similar to 
% istep. They can be used if users want to extract history production 
% data because ther can store previous updating time step. step represents 
% every time step in simulation process.

while t < tu    
   
    ts_real = ts_real + 1; 
    % When iter is 1, store ts_real. 
    if  iter == 1  
         ts_real_or   = ts_real;
    end
  
     step = dt(ts_real); if t + dt(ts_real) > tu, step = tu-t; end  
     t = t + step;  time = t;

if ~exist('timeSteps','var')
     timeSteps = [];
end
     timeSteps = [timeSteps; time]; istep = istep + 1;     
end

 %---------------------------------------------------------------------------
 % store production data time series (rates in m^3/s, pressures in Pa)
 %---------------------------------------------------------------------------    
 % simulate truth <= REMOVED
 
 % simulate ensemble
for j = 1 : nmembers          
           if t_or == 0           
           statesE_new{j} = model(cumsum(dt(1:istep)),pE{j});
           else
           statesE_new{j} = model(cumsum(dt(1:istep)),pE{j});
           end
end
%storeProduction_comp;
%clear rate flow pressure fracflow 

disp(['finished all simulations. time = ' num2str(time)])   