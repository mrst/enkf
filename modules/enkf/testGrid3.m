%% Real field model example
%
%----------------------------------------------------------------------------------
% SYNOPSIS:
%
% DESCRIPTION:
%
%   Norne is an oil and gas field lies located in the Norwegian Sea. The
%   reservoir is found in Jurrasic sandstone at a depth of 2500 meter below
%   sea level. Operator Statoil and partners (ENI and Petoro) have agreed
%   with NTNU to release large amounts of subsurface data from the Norne
%   field for research and education purposes.  The
%   <http://www.ipt.ntnu.no/~norne/wiki/doku.php Norne Benchmark> datasets
%   are hosted and supported by the Center for Integrated Operations in the
%   Petroleum Industry (IO Center) at NTNU. Recently, the
%   <http://www.opm-project.org OPM Initiative> released the full simulation
%   model as an open data set on <https://github.com/OPM/opm-data GitHub>. 
%   Random permeability realizations are generated.
%{
  Copyright TNO, Applied Geosciences.

  This file is part of the EnKF module for MRST. 

  The EnKF module is free software. You can redistribute it and/or modify it under 
  the terms of the GNU General Public License as published by the Free Software 
  Foundation, either version 3 of the License, or (at your option) any later version.
 
  The EnKF module is distributed in the hope that it will be useful, but WITHOUT 
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along with this 
  code.  If not, see <http://www.gnu.org/licenses/>.
%}
%
% The code has been tested with Matlab version R2014b.
%
% Please acknowledge TNO in any reports or publications that use the EnKF module. 
% TNO welcomes any feedback, improvements and extensions to the code.
%
% Written by Olwijn Leeuwenburgh, TNO.
%----------------------------------------------------------------------------------
%linsolve = @(S,b) agmg(S, b, [], 1e-10, [], 1);
linsolve = @mldivide;

try
    require deckformat
catch %#ok<CTCH>
    mrstModule add deckformat
end

%% ONLY FOR TESTING
% ne = 2; j = 1;

%% Check for existence of input model data
%grdecl = fullfile(ROOTDIR, 'examples/data/Norne','NORNE.GRDECL'); %'NORNE_UPSCALED.GRDECL'); %
grdecl = [ROOTDIR filesep 'examples' filesep 'data' filesep 'Norne' filesep 'NORNE.GRDECL'];
if ~exist(grdecl, 'file')
   error('Model data is not available.')
end

%% Read and process the model
grdecl = readGRDECL(grdecl);
usys   = getUnitSystem('METRIC');
grdecl = convertInputUnits(grdecl, usys);

% select first layer only
actnum = reshape(grdecl.ACTNUM,grdecl.cartDims); actnum(:,:,2:end) = 0;
grdecl.ACTNUM = reshape(actnum,prod(grdecl.cartDims),1); clear actnum

% actnum = grdecl.ACTNUM; actnum(grdecl.PORO < 0.01) = 0;
% actnum = reshape(actnum,grdecl.cartDims);
% actnum(:,:,4:5) = 0; actnum(:,1:12,:) = 0;
% % actnum(27:29,100:102,4:5) = 0; actnum(30,19,:) = 0; 
% actnum = reshape(actnum,numel(actnum),1);
% grdecl.ACTNUM = actnum; clear actnum

G = processGRDECL(grdecl,'checkgrid', false);
% G = initEclipseGrid(grdecl);

% clf
% pargs = {'EdgeAlpha'; 0.1; 'EdgeColor'; 'k'};
% plotGrid(G,'FaceColor','none', pargs{:});
% plotFaces(G,find(G.faces.tag>0), ...
%    'FaceColor','red','FaceAlpha',0.2, 'EdgeColor','r','EdgeAlpha',0.1);
% axis off; view(-155,80); zoom(1.7);

G = computeGeometry(G(1));

faces = find(G.faces.tag>0);
actnum = grdecl.ACTNUM; clear grdecl;

%% Set rock and fluid data

gravity off

% for jj = 1 : ne + 1
%     kx = logNormLayers(G.cartDims, 100*[1:G.cartDims(3)], 'sz', [11, 11, 1]);
% 
%     kx = kx(G.cells.indexMap);
%     kx = 2 + (kx-min(kx))/(max(kx)-min(kx))*4000;
%    
%     K(:,jj) = kx;
%     phi(:,jj) = 0.25*ones(size(kx));
%     
% end

nx = 47; ny = 112; nz = 1; ng = nx * ny * nz; nr = 1000;
%file = fullfile(ROOTDIR,'examples/datasets/norne','sgsim_r25_17_a20_n1000.gslib');
file =  [ROOTDIR 'examples' filesep 'datasets' filesep 'norne' filesep 'sgsim_r25_17_a20_n1000.gslib'];
fid=fopen(file);
data = fscanf(fid, '%g', nr * ng);
fclose(fid);
data = reshape(data, nr, nx * ny * nz);
kmin = min(min(data)); kmax = max(max(data));
for jj = 1 : ne + 1
    dummy = reshape(data(jj,:)',nx,ny,nz);
    dummy = dummy(1:G.cartDims(1),1:G.cartDims(2),1);
    dummy = reshape(dummy(1:G.cartDims(1),1:G.cartDims(2),1),G.cartDims(1)*G.cartDims(2),1);
    kx = [];
    for l = 1:G.cartDims(3)
        kx = [kx; dummy];
    end
    kx = kx(G.cells.indexMap);   
    kx = 80 * exp(1.5 * kx); %0.75
    K(:,jj) = kx;
    phi(:,jj) = 0.25 * (kx ./200).^0.1;
end

K = convertFrom(K, milli*darcy);

rock.perm = [K(:,end) K(:,end) K(:,end)/10];
rock.poro = phi(:,end); 
                   
fluid = initSimpleADIFluid('mu' , [   0.4,  0.9]*centi*poise  , ...
                           'rho', [1014, 859]*kilogram/meter^3, ...
                           'n'  , [   4,   4]                 , ...
                           'smin' , [ 0.2, 0.2]                 , ...
                           'cR',    1e-5/barsa, ...
                           'phases', 'wo');  
   
%% Initial state
p0 = 350*barsa(); %400*barsa();
s0 = 0.2;

%% Introduce wells

% Set vertical injectors
I = [ 9, 26,  8, 25, 35, 10];
J = [14, 14, 35, 35, 68, 75];
bhp = 350*barsa(); q = -1; %150*meter^3/day;
radius = 0.1;
comp = [1,0];
for k = 1 : numel(I)
    name = ['I' int2str(k)];
    wells{k} = {I(k),J(k),q,bhp,radius,comp,name};
end

% Set vertical producers
nW = length(wells);
I = [17, 12, 25, 35, 15];
J = [23, 51, 51, 95, 94];
bhp = 200*barsa(); q = -1;
radius = 0.1;
comp = [0,1];
for k = 1 : numel(I)
    name = ['P' int2str(k)];
    wells{nW+k} = {I(k),J(k),q,bhp,radius,comp,name};
end
