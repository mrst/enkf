%% Real field model example
%
%----------------------------------------------------------------------------------
% SYNOPSIS:
%
% DESCRIPTION:
% This is an experimental example which represents the test grid model.

%{
  Copyright TNO, Applied Geosciences.

  This file is part of the EnKF module for MRST. 

  The EnKF module is free software. You can redistribute it and/or modify it under 
  the terms of the GNU General Public License as published by the Free Software 
  Foundation, either version 3 of the License, or (at your option) any later version.
 
  The EnKF module is distributed in the hope that it will be useful, but WITHOUT 
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along with this 
  code.  If not, see <http://www.gnu.org/licenses/>.
%}
%
% The code has been tested with Matlab version R2014b.
%
% Please acknowledge TNO in any reports or publications that use the EnKF module. 
% TNO welcomes any feedback, improvements and extensions to the code.
%
% Written by Olwijn Leeuwenburgh, TNO.
%----------------------------------------------------------------------------------
%linsolve = @(S,b) agmg(S, b, [], 1e-10, [], 1);
linsolve = @mldivide;
%% model definition

% grid structure
L = 310;
G = cartGrid([10, 1, 16], [L, L/5 ,L/5]*meter^3);
G = computeGeometry(G);

 
%% Set rock and fluid data
% rock_real = getSPE10rock(1:40, 101:140, 4);

% % Modification of rock properties to get a low heterogeneous reservoir.
p = gaussianField(G.cartDims, [0.2 0.4], [11 3 3], 2.5);
K = p.^3.*(1e-5)^2./(0.81*72*(1-p).^2);
rock_real = makeRock(G, K(:), p(:));
poro_real  = rock_real.poro;
perm_real = rock_real.perm;
% % Generate some random distributed permeability for different ensembles
% perm = ones(G.cartDims(1)*G.cartDims(2)*G.cartDims(3),ne+1);
% poro = ones(G.cartDims(1)*G.cartDims(2)*G.cartDims(3),ne+1);
% 

% % Generate some layer permeability field
%  for i = 1 : ne
%  perm_layer(:,i) = reshape(gaussianField([ 1 1 G.cartDims(3)], [1*milli*darcy 10000*milli*darcy], [11 3 3], 5.5),[1 * 1 * G.cartDims(3),1]);
%      for j = 1 : G.cartDims(3)
%          % assign layered permeability field
%        perm(G.cartDims(1)*G.cartDims(2)*(j-1)+1:G.cartDims(1)*G.cartDims(2)*j,i) = perm_layer(j,i) * ones(G.cartDims(1)*G.cartDims(2),1);
%      end   
%   end  

% % Generate ensemble porosity field for every layer
%  for i = 1 : G.cartDims(3)
%      poro_layer(i,1:ne) = reshape(gaussianField([ 1 1 ne], [0.4 0.8], [11 3 3], 5.5),[ne,1]);
%      % Assign layerd properties for each layer.
%      for j = 1 : ne
%      poro((i-1) * G.cartDims(1)+1 : i * G.cartDims(1), j) = poro_layer(i,j);
%      end
%  end  

% Load rock properties from .mat file.
load([ROOTDIR  filesep 'examples' filesep 'datasets' filesep 'SPE10' filesep 'random_Perm_Por_Field.mat']); 
% Optional location for enkf/examples/datasets/rock_Egg.mat
% load([ROOTDIR 'modules' filesep 'enkf' filesep 'examples' filesep 'datasets' filesep 'SPE10' filesep 'rock_Egg.mat']);

perm(:,ne+1) = perm_real;
poro(:,ne+1) = poro_real;

% Add reasonable rock properties
K = perm(:,1:ne+1); clear perm
phi = poro(:,1:ne+1); clear poro

rock.perm = [K(:,end) K(:,end) K(:,end)];
rock.poro = phi(:,end);

% Fluid properties
% Generate random variables for fluidE (ne)
nwE = 2*ones(1,ne);
noE = 2*ones(1,ne);
SwE = 0.2*ones(1,ne);
SoE = 0.2*ones(1,ne);
kwE =1*ones(1,ne);
koE =1*ones(1,ne);


% Two-phase template model
    fluid = initSimpleADIFluid('mu' , [   .3,  5]*centi*poise  , ...
                           'rho', [1000, 700]*kilogram/meter^3, ...
                           'n'  , [   2,   2]                 , ...
                           'smin' , [ 0.2, 0.2]                 , ...
                           'cR',    1e-5/barsa, ...
                           'phases', 'wo');
    fluidE = cell(ne,1);

%% Introduce wells

% Set vertical injectors
I = [1,1];
J = [1,1];
ZI = [1,2; 3,4];
bhp = 1000*barsa(); q = 300*meter^3/day; %150*meter^3/day;
radius = 0.1;
comp = [1,0];
for k = 1 : numel(I)
    name = ['I' int2str(k)];
    wells{k} = {I(k),J(k),q,bhp,radius,comp,name,ZI(k,:)};
end

% Set vertical producers
nW = length(wells);
I = [G.cartDims(1), G.cartDims(1)];
J = [G.cartDims(2), G.cartDims(2)];
ZP = [3,4; 1,2];
bhp = 150*barsa(); q = -1;
radius = 0.1;
comp = [0,1];
for k = 1 : numel(I)
    name = ['P' int2str(k)];
    wells{nW+k} = {I(k),J(k),q,bhp,radius,comp,name,ZP(k,:)};
end

%% Initial state

p0 = 200*barsa(); %400*barsa();
s0 = 0.2;