%% Example demonstrating the two-phase oil-water Egg model
% This example sets up and runs the Egg model using the two-phase AD
% solvers. 
%
% For details on the EggModel and the corresponding ensamble, see
% Jansen, J. D., et al. "The egg model–a geological ensemble for reservoir
% simulation." Geoscience Data Journal 1.2 (2014): 192-195.

mrstModule add ad-core ad-blackoil deckformat diagnostics 

% Realizations can be set to 0 for base cae, or a number between 1 and 100
% for different permeabilities.sa
realization = 0;
[G, rock, fluid, deck] = setupEGG('realization', realization);
[state, model, schedule, nonlinear] = initEclipseProblemAD(deck, 'G', G, 'TimestepStrategy', 'none');


model.getPhaseNames()

schedule = simpleSchedule(schedule.step.val(5:52), 'W', schedule.control.W);

problem = packSimulationProblem(state, model, schedule, 'EGG_realization_ENKF', 'NonLinearSolver', nonlinear);

%%
[ok, status] = simulatePackedProblem(problem);


%% Run simulation
 [wellSols, states, reports] = getPackedSimulatorOutput(problem);
 for j =  1:numel(wellSols)
     for i = 1:8
         observation.data(j,i) = wellSols{j}(i).bhp;
     end
     for i = 9:12
         observation.data(j,i) = wellSols{j}(i).qOs;
     end
     for i = 9:12
         observation.data(j,i+4) = wellSols{j}(i).qWs;
     end
 end
    observation.time = cumsum(problem.SimulatorSetup.schedule.step.val);
 % d = PostProcessDiagnosticsMRST(problem);
