%% Load 

EXACT_GRADIENT = '/home/manuel/repos/spe_rsc_2021_datadriven_models/Egg_Model/Exact_Gradient_Example_Paper.mat';
INEXACT_GRADIENT = '/home/manuel/repos/spe_rsc_2021_datadriven_models/Egg_Model/Inexact_Gradient_Example_Paper.mat';

Exact_gradient = load(EXACT_GRADIENT,'wellSols_ref','schedule','weighting','model','obj_scaling','history');
Inexact_gradient = load(INEXACT_GRADIENT,'history');

wellSols_ref = Exact_gradient.wellSols_ref;
schedule = Exact_gradient.schedule;
weighting = Exact_gradient.weighting;
model = Exact_gradient.model;
objScaling = Exact_gradient.obj_scaling;


hystory_exact   = (objScaling -objScaling*Exact_gradient.history.val);
hystory_inexact = (objScaling -objScaling*Inexact_gradient.history.val);

iter_linesearch_exact = Exact_gradient.history.lsit;
iter_linesearch_inexact = Inexact_gradient.history.lsit;

%%

                 
colol{6}=   [0, 0.4470, 0.7410];
 %colol{5}=   [0.8500, 0.3250, 0.0980];	          	
 colol{4}=   [0.9290, 0.6940, 0.1250];	          
 colol{5}=   [0.4940, 0.1840, 0.5560];	          	
 colol{2}=   [0.4660, 0.6740, 0.1880];	          	
 colol{3}=   [0.3010, 0.7450, 0.9330];	          	
 colol{1}=   [0.6350, 0.0780, 0.1840];
           
%  colol{1}=   [240,128, 128]/255;
%  colol{2}=   [152,251,152]/255;
%   colol{3}=   [175,238,238]/255;	          	
% 
%  colol{4}=   [238,232,170]/255;
%  
%  colol{5}=   [216,191,216]/255;	          	
%  colol{6}=   [0.3010, 0.7450, 0.9330];	          	
%  colol{7}=   [0.6350, 0.0780, 0.1840];
            
%Nnsembles =  [5 10 20 40 80 160];
Nnsembles =  [160,80,40,20,10];

for kk= 1 :length(Nnsembles)

    number_of_ensembles = Nnsembles(kk);
     input_file = ['output_FD_',num2str(number_of_ensembles),'.m'];
    [~ ,name, ~] = fileparts(input_file);
    workdir = [mrstOutputDirectory filesep 'ENKF' filesep   convertStringsToChars(name) ];   

    iter = 4;
    step =1;

    file_index = 48*30*day();

    number_time_steps = 48;

    %% Plot bhp Injectors

         wellSols_enkf = {};

           % plot(observation.time,observation.data(:,sp),'ko')
            %For each iteration

            for i =  1:iter
                file_1_name = [workdir,'/summary_',num2str(file_index),'_iter',num2str(i),'.mat'];
                load(file_1_name);

                    for e = 1:number_of_ensembles

                        wellSols_enkf{i,e} = struct('wellSols',{});
                        %wellSols_enkf{i}{e}.wellSols ={}; 

                        for time_steps =  1:number_time_steps

                            wellSols{time_steps} =struct('bhp',[],'qWs',[],'qOs',[]);
                            wellSols_mean{time_steps} =struct('bhp',[],'qWs',[],'qOs',[]);

                            %well sp, iteration i, ensemble e
                            % BHP                         
                            for sp = 1:8
                                wellSols{time_steps}(sp).bhp = statesE_new{e}.data(time_steps,sp);
                                wellSols{time_steps}(sp).qWs = wellSols_ref{time_steps}(sp).qWs;
                                wellSols{time_steps}(sp).qOs = wellSols_ref{time_steps}(sp).qOs;                                                                
                            end

                            % OilRates and Water 
                            for sp = 9:12
                                wellSols{time_steps}(sp).bhp = wellSols_ref{time_steps}(sp).bhp;
                                wellSols{time_steps}(sp).qWs = statesE_new{e}.data(time_steps,sp+4);
                                wellSols{time_steps}(sp).qOs = statesE_new{e}.data(time_steps,sp);                                
                            end
                             wellSols_enkf{i,e} = wellSols;
                        end
                    end                            
            end


    %% Averaging
    wellSols_mean = {};
            for i =  1:iter
                    for e = 0:1:number_of_ensembles
                            for time_steps =  1:number_time_steps

                            if e == 0
                                wellSols_avg{time_steps} =struct('bhp',[],'qWs',[],'qOs',[]);                            
                            end
                            %well sp, iteration i, ensemble e
                            % BHP                         
                            for sp = 1:12
                                if e == 0
                                    wellSols_avg{time_steps}(sp).bhp = 0;
                                    wellSols_avg{time_steps}(sp).qWs = 0;
                                    wellSols_avg{time_steps}(sp).qOs = 0; 
                                else
                                    wellSols_avg{time_steps}(sp).bhp = wellSols_avg{time_steps}(sp).bhp + wellSols_enkf{i,e}{time_steps}(sp).bhp/number_of_ensembles;
                                    wellSols_avg{time_steps}(sp).qWs = wellSols_avg{time_steps}(sp).qWs + wellSols_enkf{i,e}{time_steps}(sp).qWs/number_of_ensembles;
                                    wellSols_avg{time_steps}(sp).qOs = wellSols_avg{time_steps}(sp).qOs + wellSols_enkf{i,e}{time_steps}(sp).qOs/number_of_ensembles; 
                                end
                            end
                        end
                    end
                    wellSols_mean{i} = wellSols_avg;
            end
    
    %%
             misfitVal = zeros(iter,number_of_ensembles);
                for i = 1:iter
%                     for e = 1:number_of_ensembles
%                         wellSols = wellSols_enkf{i,e};
%                         misfitVals = matchObservedOW(model.G, wellSols, schedule, wellSols_ref, weighting{:});
%                         misfitVal(i,e) = sum(vertcat(misfitVals{:})) ;        
%                     end 
                                        
                        wellSols = wellSols_mean{i};
                        misfitVals = matchObservedOW(model.G, wellSols, schedule, wellSols_ref, weighting{:});
                        misfitVal(i) = sum(vertcat(misfitVals{:})) ;                            
                end

                Iterations = (1:iter);
                

                hold on
                %for e = 1:number_of_ensembles
                    model_evaluations = number_of_ensembles*Iterations;
                    h{kk}=plot(model_evaluations,misfitVal(Iterations)','color',colol{kk},'LineWidth',2);                
                %end            
                hold off

end           
hold on

iter_linesearch_exact(1) =  1;
model_evaluations_exact = cumsum(2*iter_linesearch_exact);

iter_linesearch_inexact(1) =  1;
model_evaluations_inexact = cumsum(42*iter_linesearch_inexact);


hh=plot(model_evaluations_exact,hystory_exact,'o-r',...
                    model_evaluations_inexact,hystory_inexact,'*-b');
 hold off

            
            set(gca, 'YScale', 'log')
            set(gca, 'XScale', 'log')
            ylabel('Mean squared error');
            xlabel('Model evaluations');
            xticks([1:8,10,15 20 30 40 80 160 320 480 640])
            
            lgd=  legend([hh(1), hh(2), h{5} h{4} h{3} h{2} h{1}],{'Exact gradient','Aproximated  gradient','mean, ES-MDA, Ne =  10','mean, ES-MDA, Ne =  20','mean, ES-MDA, Ne =  40','mean, ES-MDA, Ne =  80','mean, ES-MDA, Ne =  160'});
set(gca,'Fontsize',14);
grid on
