
function problem = control2problem_enfk(u,problem,parameters)
% Convert parameter param in model to control vector
np = numel(parameters);



reel = 1;
for k = 1:np
    updateOperators = false;
    switch parameters{k}.distribution
        case 'cell' %parameter disstribution per cell
            switch parameters{k}.name
                case 'porevolume'
                    [umin, umax] = deal(parameters{k}.boxLims(1), parameters{k}.boxLims(2));
                    Indx = parameters{k}.Indx;
                    m = length(Indx);
                    problem.SimulatorSetup.model.operators.pv(Indx) = u(reel:reel+m-1)...
                        *(umax-umin)+umin;
                    reel = reel + m ;
                case 'initSw'
                   [umin, umax] = deal(parameters{k}.boxLims(1), parameters{k}.boxLims(2)); 
                   Indx = parameters{k}.Indx;
                   m = length(Indx);
                   problem.SimulatorSetup.state0.s(Indx,1) = u(reel:reel+m-1)...
                                        *(umax-umin)+umin;
                   problem.SimulatorSetup.state0.s(Indx,2) = (1-u(reel:reel+m-1))...
                                        *(umax-umin)+umin;                 
                   reel = reel + m ;                   
                %case {'swl', 'swcr', 'swu', 'sowcr'}
                case 'permeability'
                   [umin, umax] = deal(parameters{k}.boxLims(1), parameters{k}.boxLims(2)); 
                    Indx = parameters{k}.Indx;
                    m = length(Indx);
                   problem.SimulatorSetup.model.rock.perm(:,1) =  u(reel:reel+m-1)...
                                       *(umax-umin)+umin;
                   problem.SimulatorSetup.model.rock.perm(:,2) =  u(reel:reel+m-1)...
                                       *(umax-umin)+umin; 
                   problem.SimulatorSetup.model.rock.perm(:,3) =  u(reel:reel+m-1)*0.1...
                                       *(umax-umin)+umin; 
                   reel = reel + m;
                 case 'porosity'
                   [umin, umax] = deal(parameters{k}.boxLims(1), parameters{k}.boxLims(2)); 
                    Indx = parameters{k}.Indx;
                    m = length(Indx);
                   problem.SimulatorSetup.model.rock.poro(:,1) =  u(reel:reel+m-1)...
                                       *(umax-umin)+umin; 
                   reel = reel + m;
                otherwise
                    warning('Parameter %s is not implemented',parameters{k}.name)
            end
        case  'connection'
            % Each value in u should be assigned to the connection
            % described by the indices from the corresponding list in Indx
            for i = 1 : numel(parameters{k}.Indx)
                Indx = parameters{k}.Indx{i};

                % boxLims can be given per connection or only once
                [umin, umax] = deal(parameters{k}.boxLims(1,1), parameters{k}.boxLims(1,2));
                if size(parameters{k}.boxLims, 1) > 1
                    [umin, umax] = deal(parameters{k}.boxLims(i,1), parameters{k}.boxLims(i,2));
                end
                
                switch parameters{k}.name
                    case 'transmissibility'
                        problem.SimulatorSetup.model.operators.T(Indx) =  u(reel + i-1)...
                            *(umax-umin)+umin;
                    case 'porevolume'
                        problem.SimulatorSetup.model.operators.pv(Indx) =  u(reel + i-1)...
                            *(umax-umin)+umin;
                    case 'permeability'
                        if (parameters{k}.log == true)
                            problem.SimulatorSetup.model.rock.perm(Indx,1) = 10.^(u(reel + i-1)...
                                *(umax-umin)+umin);
                        else
                            problem.SimulatorSetup.model.rock.perm(Indx,1) =  u(reel + i-1)...
                                *(umax-umin)+umin;
                        end
                        updateOperators = true;
                    case 'porosity'
                        problem.SimulatorSetup.model.rock.poro(Indx,1) =  u(reel + i-1)...
                            *(umax-umin)+umin;
                        updateOperators = true;
                    otherwise
                        warning('Parameter %s is not implemented for connection', parameters{k}.name)
                end
            end
            reel = reel + numel(parameters{k}.Indx);
            
        case 'well'
            % Each value in u should be assigned to its corresponding well
            switch parameters{k}.name
                case 'conntrans'
                    [umin, umax] = deal(parameters{k}.boxLims(1), parameters{k}.boxLims(2));
                    for i=1:numel(parameters{k}.Indx)
                        problem.SimulatorSetup.schedule.control.W(parameters{k}.Indx(i)).WI = ...
                            problem.SimulatorSetup.schedule.control.W(parameters{k}.Indx(i)).WI*0 + ...
                            u(reel + i-1) * (umax-umin)+umin;
                    end
                    reel = reel + numel(parameters{k}.Indx);
                otherwise
                    warning('Parameter %s is not implemented for well', parameters{k}.name)
            end
               
        case  'general'
            switch parameters{k}.name
                case 'transmissibility'
                    [umin, umax] = deal(parameters{k}.boxLims(1), parameters{k}.boxLims(2));
                    I_Tr =  parameters{k}.Indx;
                    problem.SimulatorSetup.model.operators.T(I_Tr) = 0*problem.SimulatorSetup.model.operators.T(I_Tr) + u(k)...
                        *(umax-umin)+umin;
                case 'porevolume'
                    [umin, umax] = deal(parameters{k}.boxLims(1), parameters{k}.boxLims(2));
                    I_pv =  parameters{k}.Indx;
                    problem.SimulatorSetup.model.operators.pv(I_pv) = 0*problem.SimulatorSetup.model.operators.pv(I_pv) + u(k)...
                        *(umax-umin)+umin;
                case 'permeability'
                    [umin, umax] = deal(parameters{k}.boxLims(1), parameters{k}.boxLims(2));
                    problem.SimulatorSetup.model.rock.perm = 0*problem.SimulatorSetup.model.rock.perm + u(reel)...
                        *(umax-umin)+umin;
                    reel = reel + 1;
                    updateOperators = true;
                case 'porosity'
                    [umin, umax] = deal(parameters{k}.boxLims(1), parameters{k}.boxLims(2));
                    problem.SimulatorSetup.model.rock.poro = 0*problem.SimulatorSetup.model.rock.poro + u(reel)...
                        *(umax-umin)+umin;
                    reel = reel + 1;
                    updateOperators = true;
                case 'conntrans'
                    Indx = parameters{k}.Indx;
                    for i = 1 : size(parameters{k}.Indx,1)
                        [umin, umax] = deal(parameters{k}.boxLims(i,1), parameters{k}.boxLims(i,2));
                        problem.SimulatorSetup.schedule.control.W(Indx(i,1)).WI(Indx(i,2)) = u(reel + i-1)...
                            *(umax-umin)+umin;
                    end
                    reel = reel + size(parameters{k}.Indx,1);
                otherwise
                    warning('Parameter %s is not implemented',parameters{k}.name)
            end
        otherwise
            warning('Parameter distribution %s is not implemented',parameters{k}.distribution)
    end
    if updateOperators
        % Update operators and model
        problem.SimulatorSetup.model.operators ...
            = setupOperatorsTPFA(problem.SimulatorSetup.model.G, ...
            problem.SimulatorSetup.model.rock);
        problem.SimulatorSetup.model = problem.SimulatorSetup.model.removeStateFunctionGroupings();
    end
end
% Concatenate in a column vector

