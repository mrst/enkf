
mrstModule add ad-core ad-blackoil deckformat diagnostics mrst-gui ...
    ad-props incomp optimization dd-models enkf

 
%% Run EGG field simulation
Settup_Egg_simulation 
 
problem_ref =  problem;
model_ref    = problem.SimulatorSetup.model;
states_ref   = states;
wellSols_ref = wellSols;
schedule_ref = problem.SimulatorSetup.schedule;
W_ref        = problem.SimulatorSetup.schedule.control.W;

%%  Compute diagnostics 

 DD = WellPairNetwork(model_ref,schedule_ref,states_ref,state,wellSols_ref);
 DD =  DD.filter_wps(1*stb/day);
 DD.plotWellPairConnections()
 DD.plotWellPairsData('subplot',[4,4])
   
% Initializing parameters
for i =  1:numel(DD.wps)
    pv(i,1) = DD.wps{i}.volume;
    TT(i,1) = DD.wps{i}.Tr;
end

%% Creating data driven model

L = 435;
G = cartGrid([10, 1, numedges(DD.Graph)], [L, L/5 ,L/5]*meter^3);
G = computeGeometry(G);


fluid =  model_ref.fluid;
rock = makeRock(G, 1000*milli*darcy, 0.1);

gravity off
model = GenericBlackOilModel(G, rock, fluid);
model.gas=false;
model.OutputStateFunctions = {};

 
[model,W,indexs] = createDDmodel_1(model,10,DD.Graph,W_ref);

%
ts = schedule_ref.step.val;

state0 = initState(model.G, W , 400*barsa,[0.2, 0.8]); 

schedule = simpleSchedule(ts, 'W', W);

problem = packSimulationProblem(state0, model, schedule, 'EggModelHistMatchReference');
[ok, status] = simulatePackedProblem(problem);
[wellSols, states, reports] = getPackedSimulatorOutput(problem);

%% Preparing parameters and scaling values for each one

WellIP = [];
cell = [];
well_index = []; 
levels = 1;        
for  i = 1:12
    for l = 1 : levels
        well_index = [well_index; i,l];
    end
    WellIP = [WellIP; 2*W(i).WI(1)];
end

include_initSw = false;
initSw_cells = [];
for i=1:numel(indexs.cells)
    initSw_cells = union(initSw_cells, indexs.cells{i});
end
default_initSw = problem.SimulatorSetup.state0.s(initSw_cells,1);


% Tr_boxlimits = [0.1*TT ; 1.4*TT]';
% Pv_boxlimits = [0.01*pv/10 ; 2*pv/10]';
% well_boxlimits = [ 0.01*WellIP , ...
%                   7*WellIP];
% 

Tr_boxlimits = [0*TT+0 , 0*TT+1];
Pv_boxlimits = [0*pv/10+0 , 0*pv/10+1];
well_boxlimits = [ 0*WellIP , 0*WellIP+1];
initSw_boxlimits = [0, 1];


well_IP = struct('name','conntrans',...
                       'type','value',...
                       'boxLims', well_boxlimits,...
                       'distribution','general',...
                       'Indx',well_index);


transmisibility_conection = struct('name','transmissibility',...
                       'type','value',...
                       'boxLims', Tr_boxlimits ,...
                       'distribution','connection',...
                       'Indx',{indexs.faces});


porevolume_conection = struct('name','porevolume',...
                       'type','value',...
                       'boxLims',Pv_boxlimits,...
                       'distribution','connection',...
                       'Indx',{indexs.cells});                                                                

initSw_cell = struct('name', 'initSw', ...
                     'type', 'value', ...
                     'boxLims', initSw_boxlimits, ...
                     'distribution', 'cell', ...
                     'Indx', initSw_cells);

parameters =  {};                          
parameters{1} = transmisibility_conection;
parameters{2} = porevolume_conection ;
parameters{3} = well_IP ;

val{1} = TT;
val{2} = pv/10;
val{3} = WellIP; %2*WellIP;

default_initSw = default_initSw + 0.1*randn(size(default_initSw));
default_initSw = abs(default_initSw); % avoid negativ and exact zero values
default_initSw(default_initSw > 1) = 1.0;

if include_initSw
    val{4} = default_initSw;
    parameters{4} = initSw_cell;
end

p0_fd = value2control(val, parameters);

 
 %% Defining model
 

Model = ModelClass(problem,parameters,@control2problem_enfk,@run_mrst_model,'TimeVar','dt');
            

model_forecast = Model.runModel(p0_fd*0.7,cumsum(problem.SimulatorSetup.schedule.step.val));

%% Structuring of uncertain parameters
nstat = [numedges(DD.Graph) numedges(DD.Graph) numel(W)];
if include_initSw
    nstat(4) = numel(default_initSw);
end


%% Ensembles

numEnsembleMembers = 80;
for i = 1:numEnsembleMembers
    rng('shuffle');
    pE{i} =  p0_fd.*0.6.*(rand(size(p0_fd))-0.5) + p0_fd;
    pE{i} = fixStates(pE{i}, nstat);
end
%save('Model_Ensembles','pE');

%load('Model_Ensembles','pE');

Model.pE = pE;  

%% Specifying the parameter transformations 

% Bounded logarithmic transforms. Bounds [0 1] is ofcourse a bad idea, need
% to have a realistic upper bound if this can be used
%transformobs  = { [ 2 0 1], [ 2 0 1], [ 2 0 1] }; 
%itransformobs = { [-2 0 1], [-2 0 1], [-2 0 1] };

% Unbounded logarithmic transform. Seems to work quite good
% transformobs  = { 7  7  7 }; 
% itransformobs = {-7 -7 -7 };

% Scaled transforms
transformobs  = { 6  5  4};
itransformobs = {-6 -5 -4};

if include_initSw
    transformobs{4}  = 0;
    itransformobs{4} = 0;
end
    

  %% Running EnKf            
tic
mrstEnKF2(Model,observation,...
                         'workdir',[mrstOutputDirectory filesep 'ENKF' filesep  ['output_FD_',num2str(numel(Model.pE))]],...
                         'modelname',   'DD_Model',  ...
                         'method',      'EnKF',      ...
                         'scheme',           1,      ...
                         'nmembers',    numel(Model.pE),...  %Necesary?
                         'restart',     1,           ...
                         'inflation',   0,           ...
                         'transform', {0},          ...
                         'niterations', 4,           ...
                         'Alpha',   [9.333 7 4 2],           ...
                         'nrepeats',    0,           ...
                         'tolerance',   0.01,        ...
                         'tsim',       48*day*30,    ...   %Necesary?
                         'tend',       48*day*30,    ...   %Necesary?
                         'dt',          30*day,      ...
                         'dtu',        48*30*day,    ...
                         'dts',         2*30*day,    ...
                         'dto',         2*30*day,    ...
                         'transformobs',  transformobs,    ... % transformation functions for the states  % see applyTransform.m for details
                         'itransformobs', itransformobs,   ...
                         'nstat',        nstat,      ...
                         'obsnum',      numel(observation.time), ...  %Necesary?
                         'Sigma',      [1*barsa(),50*stb()/day()], ...
                         'obstype',   {'bhp','flx'},...
                         'wellobs_indx', {{1:8},{9:16}});  
plot_enkf
toc
 