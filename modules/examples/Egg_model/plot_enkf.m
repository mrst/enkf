
 input_file = "output.m";
[~ ,name, ~] = fileparts(input_file);
workdir = [mrstOutputDirectory filesep 'ENKF' filesep   convertStringsToChars(name) ];   

iter = 8;
 cmap = colormap(cool(iter));

file_index = 48*30*day();

if exist('plot_single_model_forecast') ~= 1 || exist('model_forecast') ~= 1
    plot_single_model_forecast = false;
end
    

%% Plot bhp Injectors
for sp = 1:8
subplot(2,4,sp)
        colol = 'brcm';

       % plot(observation.time,observation.data(:,sp),'ko')
          
        for i =  1:iter
            file_1_name = [workdir,'/summary_',num2str(file_index),'_iter',num2str(i),'.mat'];
            load(file_1_name);
            for e = 1: numel(statesE_new)
                hold on                
                h = plot(convertTo(timeSteps,day()),convertTo(statesE_new{e}.data(:,sp),barsa()),'-k');
                cl=cmap(i,:) ;
                set(h,'Linestyle','-','Linewidth',1,'Color',cl,'Marker','none','MarkerSize',2,'MarkerFaceColor',cl,'MarkerEdgeColor',cl);
                hold off        
            end            
           
        end
        hold on
        h_obs = plot(convertTo(timeSteps, day()),convertTo(observation.data(:,sp),barsa()),'k-');
        if plot_single_model_forecast
            h_single = plot(convertTo(model_forecast.time, day()), ...
                convertTo(model_forecast.data(:, sp), barsa()), 'r-');
        end
        hold off
        xlabel('Time [day]')
        ylabel('BHP[barsa]')
        title(['Well ',num2str(sp)])
        
        if plot_single_model_forecast
            lgd = legend([h, h_obs, h_single], ...
                {'Last iterations', 'Observations', 'single forecast'});
        else
            lgd = legend([h h_obs],{'Last iterations', 'Observations'});
        end
end

figure

%% Plot Oil rates producer
for sp = 9:12
subplot(2,4,sp-8)
        colol = 'brcm';

       % plot(observation.time,observation.data(:,sp),'ko')
          
        for i =  1:iter
            file_1_name = [workdir,'/summary_',num2str(file_index),'_iter',num2str(i),'.mat'];
            load(file_1_name);
            for e = 1: numel(statesE_new)
                hold on                
                h = plot(convertTo(timeSteps,day()),convertTo(-statesE_new{e}.data(:,sp),stb()/day()),'-k');
                cl=cmap(i,:) ;
                set(h,'Linestyle','-','Linewidth',1,'Color',cl,'Marker','none','MarkerSize',2,'MarkerFaceColor',cl,'MarkerEdgeColor',cl);
                hold off        
            end            
           
        end
        hold on
        h_obs = plot(convertTo(timeSteps, day()),convertTo(-observation.data(:,sp),stb()/day()),'k-');
        if plot_single_model_forecast
            h_single = plot(convertTo(model_forecast.time, day()), ...
                convertTo(-model_forecast.data(:, sp), stb()/day()), 'r-');
        end
        hold off
        xlabel('Time [day]')
        ylabel('Oil Rate [stb/day]')
        title(['Producer ',num2str(sp-8)])
        
        if plot_single_model_forecast
            lgd = legend([h, h_obs, h_single], ...
                {'Last iterations', 'Observations', 'single forecast'});
        else
            lgd = legend([h h_obs],{'Last iterations', 'Observations'});
        end
end


for sp = 13:16
subplot(2,4,sp-8)
        colol = 'brcm';

       % plot(observation.time,observation.data(:,sp),'ko')
          
        for i =  1:iter
            file_1_name = [workdir,'/summary_',num2str(file_index),'_iter',num2str(i),'.mat'];
            load(file_1_name);
            for e = 1: numel(statesE_new)
                hold on                
                h = plot(convertTo(timeSteps,day()),convertTo(-statesE_new{e}.data(:,sp),stb()/day()),'-k');
                cl=cmap(i,:) ;
                set(h,'Linestyle','-','Linewidth',1,'Color',cl,'Marker','none','MarkerSize',2,'MarkerFaceColor',cl,'MarkerEdgeColor',cl);
                hold off        
            end            
           
        end
        hold on
        h_obs = plot(convertTo(timeSteps, day()),convertTo(-observation.data(:,sp),stb()/day()),'k-');
        if plot_single_model_forecast
            h_single = plot(convertTo(model_forecast.time, day()), ...
                convertTo(-model_forecast.data(:, sp), stb()/day()), 'r-');
        end
        hold off
        xlabel('Time [day]')
        ylabel('Water Rate [stb/day]')
        title(['Producer ',num2str(sp-12)])
        
        
        if plot_single_model_forecast
            lgd = legend([h, h_obs, h_single], ...
                {'Last iterations', 'Observations', 'single forecast'});
        else
            lgd = legend([h h_obs],{'Last iterations', 'Observations'});
        end
end

