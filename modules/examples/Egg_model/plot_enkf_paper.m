
EXACT_GRADIENT = '/home/manuel/repos/spe_rsc_2021_datadriven_models/Egg_Model/Exact_Gradient_Example_Paper.mat';
INEXACT_GRADIENT = '/home/manuel/repos/spe_rsc_2021_datadriven_models/Egg_Model/Inexact_Gradient_Example_Paper.mat';
                    
Exact_gradient = load(EXACT_GRADIENT,'wellSols_opt','wellSols_ref','wellSols_0','model');
Inexact_gradient = load(INEXACT_GRADIENT,'wellSols_opt');

wellSols_ref = Exact_gradient.wellSols_ref;
wellSols_exact = Exact_gradient.wellSols_opt;
wellSols_0 = Exact_gradient.wellSols_0;

wellSols_inexact = Inexact_gradient.wellSols_opt;



input_file =  ['output_FD_',num2str(80),'.m'];
[~ ,name, ~] = fileparts(input_file);
workdir = [mrstOutputDirectory filesep 'ENKF' filesep   convertStringsToChars(name) ];   

iter = 5;
step =4;
 cmap = colormap(cool(iter));

file_index = 48*30*day();

       alpha =0.75;
       colol{1} =[0,0,0]+0.850;
       colol{2} =[0,0,0]+0.6;

Linewidth  = 2;
%% Plot bhp Injectors

for sp = 1:8
subplot(2,4,sp)
       % colol = 'brcm';
       % plot(observation.time,observation.data(:,sp),'ko')
            file_1_name = [workdir,'/summary_',num2str(file_index),'_iter',num2str(1),'.mat'];
            load(file_1_name);
            hold on                
            for e = 1: numel(statesE_new)
                h1 = plot(convertTo(timeSteps,day()),convertTo(statesE_new{e}.data(:,sp),barsa()),'-k');
                set(h1,'Linestyle','-','Linewidth',Linewidth,'Color',colol{1},'Marker','none','MarkerSize',1);
                       
            end
      
            file_2_name = [workdir,'/summary_',num2str(file_index),'_iter',num2str(4),'.mat'];
            load(file_2_name);
            for e = 1: numel(statesE_new)
                             
                h2 = plot(convertTo(timeSteps,day()),convertTo(statesE_new{e}.data(:,sp),barsa()),'-k');
                set(h2,'Linestyle','-','Linewidth',Linewidth,'Color',colol{2},'Marker','none','MarkerSize',1);
            end  

          h_obs = plot(convertTo(timeSteps, day()),convertTo(cellfun(@(x) x(sp).bhp, wellSols_0),barsa()),'--k',...
                       convertTo(timeSteps, day()),convertTo(cellfun(@(x) x(sp).bhp, wellSols_ref),barsa()),'-k',...
                       convertTo(timeSteps, day()),convertTo(cellfun(@(x) x(sp).bhp, wellSols_exact),barsa()),'-or',...
                       convertTo(timeSteps, day()),convertTo(cellfun(@(x) x(sp).bhp, wellSols_inexact),barsa()),'-sb');
         set(h_obs(1),'Linewidth',2);
         set(h_obs(2),'Linewidth',2);

         set(h_obs(3),'Linewidth',1);
         set(h_obs(4),'Linewidth',1);

                   
        hold off
        xlabel('Time [day]')
        ylabel('BHP[barsa]')
        title(['INJECT',num2str(sp)])
        
        
       %lgd=  legend([h1, h2, h_obs(1) h_obs(2) h_obs(3) h_obs(4)],{'Initial ensemble','Last ensemble','Initial model','Observations', 'Exact gradient','Aproximated  gradient'});
       lgd=  legend([h_obs(2), h_obs(1), h1, h2,   h_obs(3) h_obs(4)],{'Reference data','Initial reduced model',  'ES-MDA prior', 'ES-MDA posterior', 'Exact gradient','Aproximated  gradient'});

end

figure

% %% Plot Oil rates producer

for sp = 9:12
 subplot(2,4,sp-8)
       % colol = 'brcm';
       % plot(observation.time,observation.data(:,sp),'ko')
            file_1_name = [workdir,'/summary_',num2str(file_index),'_iter',num2str(1),'.mat'];
            load(file_1_name);
            hold on                
            for e = 1: numel(statesE_new)
                h1 = plot(convertTo(timeSteps,day()),convertTo(-statesE_new{e}.data(:,sp),stb()/day()),'-k');
                set(h1,'Linestyle','-','Linewidth',Linewidth,'Color',colol{1},'Marker','none','MarkerSize',1);
                       
            end
      
            file_2_name = [workdir,'/summary_',num2str(file_index),'_iter',num2str(4),'.mat'];
            load(file_2_name);
            for e = 1: numel(statesE_new)
                             
                h2 = plot(convertTo(timeSteps,day()),convertTo(-statesE_new{e}.data(:,sp),stb()/day()),'-k');
                set(h2,'Linestyle','-','Linewidth',Linewidth,'Color',colol{2},'Marker','none','MarkerSize',1);
            end  

          h_obs = plot(convertTo(timeSteps, day()),-convertTo(cellfun(@(x) x(sp).qOs, wellSols_0),stb()/day()),'--k',...
                       convertTo(timeSteps, day()),-convertTo(cellfun(@(x) x(sp).qOs, wellSols_ref),stb()/day()),'-k',...
                       convertTo(timeSteps, day()),-convertTo(cellfun(@(x) x(sp).qOs, wellSols_exact),stb()/day()),'-or',...
                       convertTo(timeSteps, day()),-convertTo(cellfun(@(x) x(sp).qOs, wellSols_inexact),stb()/day()),'-sb');
         set(h_obs(1),'Linewidth',2);
         set(h_obs(2),'Linewidth',2);
         set(h_obs(3),'Linewidth',1);
         set(h_obs(4),'Linewidth',1);

                   
        hold off
        xlabel('Time [day]')
        ylabel('Oil Rate [stb/day]')
        title(['PROD',num2str(sp-8)])
        
        
       lgd=  legend([h_obs(2), h_obs(1), h1, h2,   h_obs(3) h_obs(4)],{'Reference data','Initial reduced model',  'ES-MDA prior', 'ES-MDA posterior', 'Exact gradient','Aproximated  gradient'});
end



% %% Plot Water rates producer

for sp = 13:16
 subplot(2,4,sp-8)
       % colol = 'brcm';
       % plot(observation.time,observation.data(:,sp),'ko')
            file_1_name = [workdir,'/summary_',num2str(file_index),'_iter',num2str(1),'.mat'];
            load(file_1_name);
            hold on                
            for e = 1: numel(statesE_new)
                h1 = plot(convertTo(timeSteps,day()),convertTo(-statesE_new{e}.data(:,sp),stb()/day()),'-k');
                set(h1,'Linestyle','-','Linewidth',Linewidth,'Color',colol{1},'Marker','none','MarkerSize',1);
                       
            end
      
            file_2_name = [workdir,'/summary_',num2str(file_index),'_iter',num2str(4),'.mat'];
            load(file_2_name);
            for e = 1: numel(statesE_new)
                             
                h2 = plot(convertTo(timeSteps,day()),convertTo(-statesE_new{e}.data(:,sp),stb()/day()),'-k');
                set(h2,'Linestyle','-','Linewidth',Linewidth,'Color',colol{2},'Marker','none','MarkerSize',1);
            end  

          h_obs = plot(convertTo(timeSteps, day()),-convertTo(cellfun(@(x) x(sp-4).qWs, wellSols_0),stb()/day()),'--k',...
                       convertTo(timeSteps, day()),-convertTo(cellfun(@(x) x(sp-4).qWs, wellSols_ref),stb()/day()),'-k',...
                       convertTo(timeSteps, day()),-convertTo(cellfun(@(x) x(sp-4).qWs, wellSols_exact),stb()/day()),'-or',...
                       convertTo(timeSteps, day()),-convertTo(cellfun(@(x) x(sp-4).qWs, wellSols_inexact),stb()/day()),'-sb');
         set(h_obs(1),'Linewidth',2);
         set(h_obs(2),'Linewidth',2);
         set(h_obs(3),'Linewidth',1);
         set(h_obs(4),'Linewidth',1);

                   
        hold off
        xlabel('Time [day]')
        ylabel('Water Rate [stb/day]')
        title(['PROD',num2str(sp-12)])
        
        
       lgd=  legend([h_obs(2), h_obs(1), h1, h2,   h_obs(3) h_obs(4)],{'Reference data','Initial reduced model',  'ES-MDA prior', 'ES-MDA posterior', 'Exact gradient','Aproximated  gradient'});
end

% 
% 
% for sp = 13:16
% subplot(2,4,sp-8)
%         colol = 'brcm';
% 
%        % plot(observation.time,observation.data(:,sp),'ko')
%           
%         for i =  1:step:iter
%             file_1_name = [workdir,'/summary_',num2str(file_index),'_iter',num2str(i),'.mat'];
%             load(file_1_name);
%             for e = 1: numel(statesE_new)
%                 hold on                
%                 h = plot(convertTo(timeSteps,day()),convertTo(-statesE_new{e}.data(:,sp),stb()/day()),'-k');
%                 cl=cmap(i,:) ;
%                 set(h,'Linestyle','-','Linewidth',1,'Color',cl,'Marker','none','MarkerSize',2,'MarkerFaceColor',cl,'MarkerEdgeColor',cl);
%                 hold off        
%             end            
%            
%         end
%         hold on
%         h_obs = plot(convertTo(timeSteps, day()),convertTo(-observation.data(:,sp),stb()/day()),'k-');
%         hold off
%         xlabel('Time [day]')
%         ylabel('Water Rate [stb/day]')
%         title(['Producer ',num2str(sp-12)])
%         
%         
%        lgd=  legend([h h_obs],{'Last iterations', 'Observations'});
% end
% 
