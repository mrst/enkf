function [model_forecast,varargout] = run_mrst_model(p,parameters,problem_in,dt)
          
            problem = control2problem_enfk(p,problem_in, parameters);
            
            model = problem.SimulatorSetup.model;
            state0 = initState(model.G,...
                               problem.SimulatorSetup.schedule.control.W,...
                               problem.SimulatorSetup.state0.pressure,...
                               problem.SimulatorSetup.state0.s); 
                               
            schedule = simpleSchedule(dt, 'W', problem.SimulatorSetup.schedule.control.W);
            
            
            
            
            [wellSols, states] = simulateScheduleAD(state0, model, schedule);
        
             for j =  1:numel(wellSols)
                 for i = 1:8
                     model_forecast.data(j,i) = wellSols{j}(i).bhp;
                 end
                 for i = 9:12
                     model_forecast.data(j,i) = wellSols{j}(i).qOs;
                 end
                 for i = 9:12
                     model_forecast.data(j,i+4) = wellSols{j}(i).qWs;
                 end
             end
                model_forecast.time = cumsum(dt);
            if nargout>1
                varargout{1} = states;
            end
end

