mrstModule add incomp

ne = 10; 

testGrid1

%% Lines 111-128 from initializeModel_comp.m
   % Define wells, well control targets and compute grid connections
   W = [];
   g = G; 
   r = rock;
   
% We use MATLAB(R)'s built-in <matlab:doc('mldivide') |mldivide|>
% ("backslash") linear solver software to resolve all systems of linear
% equations that arise in the various discretzations.  In many cases, a
% multigrid solver such as Notay's AGMG package may be prefereble.  We
% refer the reader to Notay's home page at
% http://homepages.ulb.ac.be/~ynotay/AGMG/ for additional details on this
% package.   

   linsolve_p = @mldivide;  % Pressure
   linsolve_t = @mldivide;  % Transport (implicit)
   
   for iw = 1 : length(wells)
     X = wells{iw}{1}; Y = wells{iw}{2};  Z =  1:g.cartDims(3); 
     
     if length(wells{iw})== 8
     Z = wells{iw}{8};
     end
     
     type = 'rate'; val = wells{iw}{3}; 
     if wells{iw}{3}<0, type = 'bhp'; val = wells{iw}{4}; end
     radius = wells{iw}{5}; comp = wells{iw}{6}; name = wells{iw}{7};
     W = verticalWell(W, g, r, X, Y, Z, 'Type', type, ...  
         'Val', val, 'Radius', radius, 'Comp_i', comp, 'name', name);
   end

%% Initializing    
         trans = computeTrans(G, rock);         
         % initial state
         rSol = initState(G, W, p0, [0.2,0.8]);                 
         % solve initial pressure
         rSol = incompTPFA(rSol, G, trans, fluid, 'wells', W, ...
                     'LinSolve', linsolve_p);
                 
                 
%% 
tend           =  20.00*year();    % history match end time
 dt             =  0.50*year();    % simulation time step
            t = 0;

        istep = 1;
        while t < tend

             step = dt; if t + dt > tend, step = tend-t; end

             % simulate truth 
             rSol = implicitTransport(rSol, G, step, rock, fluid, ...
                             'wells', W, 'LinSolve', linsolve_t);
             % Check for inconsistent saturations
             assert(max(rSol.s(:,1)) < 1+eps && min(rSol.s(:,1)) > -eps);
             % Update solution of pressure equation
             rSol = incompTPFA(rSol, G, trans, fluid, 'wells', W, ...
                     'LinSolve', linsolve_p);

             time(istep) = t;
             model_obs(istep,1)= rSol.wellSol(2).flux;
             model_obs(istep,2)= rSol.wellSol(3).flux;
             model_obs(istep,3)= rSol.wellSol(4).flux;
             model_obs(istep,4)= rSol.wellSol(5).flux;
             model_obs(istep,5)= rSol.wellSol(1).pressure;

             t = t + step;  
             istep = istep + 1;
        end
%%
    observation.time = time;
    observation.data = model_obs ;
            
    
    % Model vs Observation


for i=1:size(model_obs,2)-1
    subplot (2,3,i)
    plot(observation.time/year(),observation.data(:,i),'ko');
    title(['flow ',num2str(i)]);
    legend('Observation');
    ylim([min(observation.data(:,i)),max(observation.data(:,i))])
end
   
subplot (2,3,5)
plot(observation.time/year(),observation.data(:,5),'ko');
title('bhp I1');
legend('Observation');
ylim([min(observation.data(:,5)),max(observation.data(:,5))])
         