
function problem = control2problem_incomp(u,problem,parameters)
% Convert parameter param in model to control vector
np = numel(parameters);
u =  abs(u);


reel = 1;
for k = 1:np
    switch parameters{k}.distribution
        case 'cell' %parameter disstribution per cell
            switch parameters{k}.name
                case 'permeability'
                   [umin, umax] = deal(parameters{k}.boxLims(1), parameters{k}.boxLims(2)); 
                    Indx = parameters{k}.Indx;
                    m = length(Indx);
                   problem.rock.perm(:,1) =  u(reel:reel+m-1)...
                                       *(umax-umin)+umin;
                   problem.rock.perm(:,2) =  u(reel:reel+m-1)...
                                       *(umax-umin)+umin; 
                   problem.rock.perm(:,3) =  u(reel:reel+m-1)*0.1...
                                       *(umax-umin)+umin; 
                   reel = reel + m;
                 case 'porosity'
                   [umin, umax] = deal(parameters{k}.boxLims(1), parameters{k}.boxLims(2)); 
                    Indx = parameters{k}.Indx;
                    m = length(Indx);
                   problem.rock.poro(:,1) =  u(reel:reel+m-1)...
                                       *(umax-umin)+umin; 
                   reel = reel + m;
                otherwise
                   warning('Parameter %s is not implemented',param{k})
            end
        case  'connection'
            switch parameters{k}.name
                otherwise
                   warning('Parameter %s is not implemented',param{k})
            end
      case  'general'    
             switch parameters{k}.name
                 otherwise
                   warning('Parameter %s is not implemented',param{k}) 
             end
      otherwise
           warning('Parameter distribution %s is not implemented',paramDist{k})
      end
end
% Concatenate in a column vector

