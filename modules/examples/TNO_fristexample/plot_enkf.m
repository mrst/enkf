
 input_file = "output.m";
[~ ,name, ~] = fileparts(input_file);
workdir = [mrstOutputDirectory filesep 'ENKF' filesep   convertStringsToChars(name) ];   

iter = 5;
 cmap = colormap(cool(iter));

file_index = 20*year();

for sp = 1:5
subplot(2,3,sp)
        colol = 'brcm';

       % plot(observation.time,observation.data(:,sp),'ko')
          
        for i =  1:iter
            file_1_name = [workdir,'/summary_',num2str(file_index),'_iter',num2str(i),'.mat'];
            load(file_1_name);
            for e = 1: numel(statesE_new)
                hold on                
                h = plot(convertTo(timeSteps,day()),statesE_new{e}.data(:,sp),'-k');
                cl=cmap(i,:) ;
                set(h,'Linestyle','-','Linewidth',1,'Color',cl,'Marker','none','MarkerSize',2,'MarkerFaceColor',cl,'MarkerEdgeColor',cl);
                hold off        
            end           
        end
        hold on
        plot(convertTo(timeSteps,day()),observation.data(:,sp),'k-')
        hold off
end

