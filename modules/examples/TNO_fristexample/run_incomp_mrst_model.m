function [model_forecast,varargout] = run_incomp_mrst_model(p,parameters,problem_in,dt)
          
            problem = control2problem_incomp(p,problem_in, parameters);
            
            W = [];
            G = problem.G; 
            rock = problem.rock;
            wells = problem.wells;
            % We use MATLAB(R)'s built-in <matlab:doc('mldivide') |mldivide|>
            % ("backslash") linear solver software to resolve all systems of linear
            % equations that arise in the various discretzations.  In many cases, a
            % multigrid solver such as Notay's AGMG package may be prefereble.  We
            % refer the reader to Notay's home page at
            % http://homepages.ulb.ac.be/~ynotay/AGMG/ for additional details on this
            % package.   

               linsolve_p = @mldivide;  % Pressure
               linsolve_t = @mldivide;  % Transport (implicit)
            %% defining well structure W
            for iw = 1 : length(wells)
             X = wells{iw}{1}; Y = wells{iw}{2};  Z =  1:G.cartDims(3); 

             if length(wells{iw})== 8
             Z = wells{iw}{8};
             end

             type = 'rate'; val = wells{iw}{3}; 
             if wells{iw}{3}<0, type = 'bhp'; val = wells{iw}{4}; end
             radius = wells{iw}{5}; comp = wells{iw}{6}; name = wells{iw}{7};
             W = verticalWell(W, G, rock, X, Y, Z, 'Type', type, ...  
                 'Val', val, 'Radius', radius, 'Comp_i', comp, 'name', name);
            end
            
                %% Initializing    
             trans = computeTrans(G, rock);         
             % initial state
             p0 = 350*barsa();
             
             % Fluid properties [water, oil]
             fluid = initCoreyFluid('mu' , [   0.4,  0.9]*centi*poise  , ...
                                    'rho', [1014, 859]*kilogram/meter^3, ...
                                    'n'  , [   4,   4]                 , ...
                                    'sr' , [ 0.2, 0.2]                 , ...
                                    'kwm', [   1,   1]);    
             
             rSol = initState(G, W, p0, [0.2,0.8]);                 
             % solve initial pressure
             rSol = incompTPFA(rSol, G, trans, fluid, 'wells', W, ...
                         'LinSolve', linsolve_p);
                     
             %% Simulating
            
            istep = 1;
            T= cumsum(dt);           
            tend  = T(end);
            t = 0;
            while t < tend
                 step = dt(istep); if t + dt(istep) > tend, step = tend-t; end

                 % simulate truth 
                 rSol = implicitTransport(rSol, G, step, rock, fluid, ...
                                 'wells', W, 'LinSolve', linsolve_t);
                 % Check for inconsistent saturations
                 assert(max(rSol.s(:,1)) < 1+eps && min(rSol.s(:,1)) > -eps);
                 % Update solution of pressure equation
                 rSol = incompTPFA(rSol, G, trans, fluid, 'wells', W, ...
                         'LinSolve', linsolve_p);                
                 model_forecast.time(istep) = t;
                % model_forecast(istep,1)= rSol.wellSol(1).flux;
                 model_forecast.data(istep,1)= rSol.wellSol(2).flux;
                 model_forecast.data(istep,2)= rSol.wellSol(3).flux;
                 model_forecast.data(istep,3)= rSol.wellSol(4).flux;
                 model_forecast.data(istep,4)= rSol.wellSol(5).flux;
                 model_forecast.data(istep,5)= rSol.wellSol(1).pressure;
                
                 t = t + step;  
                 istep = istep + 1;
            end

            if nargout>1
                varargout{1} = rSol;
            end
end

