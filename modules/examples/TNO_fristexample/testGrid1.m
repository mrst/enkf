%% 2D 5-spot example with uncertain permeability.
%
% The model consists of a square horizontal reservoir section. Four
% producer sare positioned in the corners and an injector is positioned in
% the center. Permeability realizations are read from a file (see below).
%
%{
  Copyright TNO, Applied Geosciences.

  This file is part of the EnKF module for MRST. 

  The EnKF module is free software. You can redistribute it and/or modify it under 
  the terms of the GNU General Public License as published by the Free Software 
  Foundation, either version 3 of the License, or (at your option) any later version.
 
  The EnKF module is distributed in the hope that it will be useful, but WITHOUT 
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along with this 
  code.  If not, see <http://www.gnu.org/licenses/>.
%}
%
% The code has been tested with Matlab version R2014b.
%
% Please acknowledge TNO in any reports or publications that use the EnKF module. 
% TNO welcomes any feedback, improvements and extensions to the code.
%
% Written by Olwijn Leeuwenburgh, TNO.
%% model definition

    % Dimensions
    dims(1)=21; dims(2)=21; dims(3)=1; 
    physDims(1)=1000; physDims(2)=1000; physDims(3)=10;

    % Build grid
    grdecl.cartDims = reshape(dims, 1, []);
    [X, Y, Z]  = ndgrid(linspace(0, 1, dims(1) + 1), ...
                        linspace(0, 1, dims(2) + 1), ...
                        linspace(0, 1, dims(3) + 1));

    X = X .* physDims(1);
    Y = Y .* physDims(2);
    Z = Z .* physDims(3);

    % Make pilars
    lines          = zeros([prod(dims([1, 2]) + 1), 6]);
    lines(:,[1,4]) = reshape(X(:,:,[1,end]), [], 2);
    lines(:,[2,5]) = reshape(Y(:,:,[1,end]), [], 2);
    lines(:,[3,6]) = reshape(Z(:,:,[1,end]), [], 2);

    grdecl.COORD = reshape(lines', [], 1);

    % Assign z-coordinates
    ind = @(d) 1 + fix((1 : 2*dims(d)) ./ 2);
    z   = Z(ind(1), ind(2), ind(3));
    
    grdecl.ZCORN = z(:);

    % Assign active cells
    actnum = ones(dims);
    grdecl.ACTNUM = int32(actnum(:));

    % Process grid
    G = processGRDECL(grdecl);
    G = computeGeometry(G(1)); 
    clear grdecl

    % Rock properties
    load('randomgauss21x21_spherical1000.mat'); K = perm; clear perm
%	load([ROOTDIR 'examples/datasets/5spot/randomgauss21x21_spherical1000_conditioned.mat']);
    
    truth = 995;
    for i=1:max(1,ne)
        perm(:,i) = K(:,i);
    end
    perm(:,ne+1) = K(:,truth);
    K = perm; clear perm
    
    KK = K;
%   Kmin=min(min(K)); Kmax=max(max(K));
%   K = (1 + (K-Kmin)/(Kmax-Kmin)*1000);
 
    K = 10 * exp(1 * K);
    
    phi = 0.25 * (K ./200).^0.1;
    
    K = convertFrom(K, milli*darcy);
    
    rock.perm = [K(:,2) K(:,2) K(:,2)/10];
    rock.poro = phi(:,2);
    
%   clear phi

    % Fluid properties [water, oil]
    fluid = initCoreyFluid('mu' , [   0.4,  0.9]*centi*poise  , ...
                           'rho', [1014, 859]*kilogram/meter^3, ...
                           'n'  , [   4,   4]                 , ...
                           'sr' , [ 0.2, 0.2]                 , ...
                           'kwm', [   1,   1]);             

    % Initial state
    p0 = 350*barsa();
    s0 = 0.2;
    
    % Well definitions and constraints
%     W = [];
    I = 11;
    J = 11;
    q = 150*meter^3/day; %0.006*1000*meter^3/day;
    bhp = -1;
    radius = 0.1;
    comp = [1,0]; 
    for k = 1 : numel(I)
        name = ['I' int2str(k)];
        wells{k} = {I(k),J(k),q,bhp,radius,comp,name};
    end

    nW = length(wells);
    I = [1  1 21 21];
    J = [1 21  1 21];
    bhp = 300*barsa(); q = -1;
    comp = [0,1];
    for k = 1 : numel(I)
        name = ['P' int2str(k)];
        wells{nW+k} = {I(k),J(k),q,bhp,radius,comp,name};
    end
    
    % gravity
    gravity off;                       
 
    