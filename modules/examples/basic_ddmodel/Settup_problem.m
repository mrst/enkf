
mrstModule add ad-core ad-blackoil diagnostics mrst-gui ad-props incomp optimization ddmodel
 
%% Defining a model a experimental data
% Creatting data driven model

L = 435;
G = cartGrid([10, 1, 4], [L, L/5 ,L/5]*meter^3);
G = computeGeometry(G);

% Rock and fluid properties
rock = makeRock(G, 100*milli*darcy, 0.3);

% Two-phase template model
fluid = initSimpleADIFluid('mu',    [1, 5]*centi*poise, ...
                           'rho',   [1000, 700]*kilogram/meter^3, ...
                           'n',     [2, 2], ...
                           'cR',    1e-8/barsa, ...
                           'phases', 'wo', ...
                           'sMin', [.2, .2]);

% Model GenericBlackOilModel

gravity off
model = GenericBlackOilModel(G, rock, fluid);
model.gas=false;
model.OutputStateFunctions = {};


% Adding wells 
W = verticalWell([], G, rock, 1, 1, [1], ...       
            'Type', 'rate' , 'Val',   300*meter^3/day, ... %   
            'Radius', 0.1,'Name','I1','Comp_i', [1, 0],'sign',1);

W = verticalWell(W, G, rock, 1, 1, [2], ...       
            'Type', 'rate' , 'Val',   300*meter^3/day, ... %   
            'Radius', 0.1,'Name','I2','Comp_i', [1, 0],'sign',1);
        
W = verticalWell(W, G, rock, G.cartDims(1), G.cartDims(2), [3], ...      
            'Type', 'bhp' , 'Val', 150*barsa, ... %   
            'Radius', 0.1,'Name','P1','Comp_i', [0, 1],'sign', -1);
        
W = verticalWell(W, G, rock, G.cartDims(1), G.cartDims(2), [4], ...      
            'Type', 'bhp' , 'Val', 150*barsa, ... %   
            'Radius', 0.1,'Name','P2','Comp_i', [0, 1],'sign', -1);

       
 state0 = initState(G, W , 200*barsa,[0, 1]); 
 
ts = [30 30 30 30 30 30 30 30 30 30 30 30 30 30 30 30]'*day;
       
 schedule = simpleSchedule(ts, 'W', W);
[wellSols, states] = simulateScheduleAD(state0, model, schedule);

%%  Compute diagnostics and create flow-network

 DD = WellPairNetwork(model,schedule,states,state0,wellSols);
 DD =  DD.filter_wps(1*stb/day);
 DD.plotWellPairConnections()
 DD.plotWellPairsData('subplot',[4,4])
 
[model_ref,W,indexs] = createDDmodel_1(model,10,DD.Graph,W);

%% Setting up a data driven model in problem structure

 state0_ref = initState(model_ref.G, W , 200*barsa,[0, 1]); 
 schedule_ref = simpleSchedule(ts, 'W', W);

problem_ref = packSimulationProblem(state0_ref, model_ref, schedule_ref,'reference');
[ok, status] = simulatePackedProblem(problem_ref);
[wellSols_ref, states_ref, reports] = getPackedSimulatorOutput(problem_ref);


