
Settup_problem

%% Defining experimental data

observation.time = cumsum(schedule_ref.step.val);

for i = 1: numel(wellSols_ref)
    model_obs = vertcat(wellSols_ref{i}.bhp);
    observation.data(i,1) = model_obs(1); %BHP first well
end

%% Defining paramters         
well_ip        = W(1).WI(1);
well_boxlimits = [0 , 1];

      well_IP = struct('name','conntrans',...
                       'type','value',...
                       'boxLims', well_boxlimits,...
                       'distribution','general',...
                       'Indx',[1,1]); %Index corresponding to well 1
                               
                                                                
            parameters =  {}; 
            parameters{1}  =  well_IP;
            
 
 
observation = run_mrst_model(well_ip,parameters,problem_ref, problem_ref.SimulatorSetup.schedule.step.val);
observation.time = cumsum(schedule_ref.step.val);

  problem = control2problem_enfk(well_ip*0.9,problem_ref,parameters);

 
 %% Defining model
 

Model = ModelClass(problem,parameters,@control2problem_enfk,@run_mrst_model,'TimeVar','dt');
        

model_forecast = Model.runModel(well_ip*0.7,cumsum(problem.SimulatorSetup.schedule.step.val));


% Model vs Observation
figure
t_obs =  cumsum(schedule_ref.step.val);
time = cumsum(schedule.step.val);
plot(observation.time,observation.data,'*',time,model_forecast.data)
legend('Observation','Run_Model')

%% Ensembles

 for i = 1:6
    rng('shuffle');   pE{i} =  well_ip*0.4*(rand(1)-0.5) + well_ip;
 end
save('Model_Ensembles','pE');

 %load('Model_Ensembles','pE');

Model.pE = pE;  



% 

  %% Running EnKf            
    mrstEnKF2(Model,observation,...
                         'modelname',   'DD_Model',  ...
                         'method',      'EnKF',      ...
                         'scheme',           1,      ...
                         'nmembers',    numel(Model.pE),...  %Necesary?
                         'restart',     1,           ...
                         'inflation',   0,           ...
                         'transform', {0 },          ...
                         'niterations', 1,           ...
                         'Alpha',       1,           ...
                         'nrepeats',    0,           ...
                         'tolerance',   0.01,        ...
                         'tsim',       16*day*30,    ...   %Necesary?
                         'tend',       12*day*30,    ...   %Necesary?
                         'dt',          30*day,      ...
                         'dtu',         4*30*day,    ...
                         'dts',         2*30*day,    ...
                         'dto',         2*30*day,    ...
                         'transformobs', {4},    ... % transformation functions for the states  % see applyTransform.m for details
                         'itransformobs',{-4},  ...
                         'nstat',        [1],    ...     ...
                         'obsnum',      numel(observation.time), ...  %Necesary?
                         'Sigma',      2 * barsa(), ...
                         'obstype',   { 'bhp'},...
                         'wellobs_indx', {{1}});  
                     
    
    plot_enkf

