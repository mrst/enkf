
 input_file = "inputSettings_Simple.m";
[~ ,name, ~] = fileparts(input_file);
workdir = [mrstOutputDirectory filesep 'ENKF' filesep   convertStringsToChars(name) ];   


file_index = [4 8 12 16];
colol = 'brcm';

plot(observation.time,observation.data,'ok')

    hold on
for i =  1: numel(file_index)
    file_1_name = [workdir,'/summary_',num2str(t_obs(file_index(i))),'_iter1.mat'];
    load(file_1_name);
    for e = 1: numel(statesE_new)
        plot(timeSteps,statesE_new{e},['--',colol(i)])
    end
    
     if i<numel(file_index)
         file_2_name = [workdir,'/summary_',num2str(t_obs(file_index(i))),'_iter2.mat'];
         load(file_2_name);
             for e = 1: numel(statesE_new)
                 plot(timeSteps,statesE_new{e},['s',colol(i)])
             end
             
     end
     
%      if i<numel(file_index)
%          file_3_name = [workdir,'/summary_',num2str(t_obs(file_index(i))),'_iter3.mat'];
%          load(file_3_name);
%              for e = 1: numel(statesE_new)
%                  plot(timeSteps,statesE_new{e},['v',colol(i)])
%              end
%              
%      end
     
end
    hold off



figure
        hold on

for i =  1: numel(file_index)
    file_1_name = [workdir,'/states_',num2str(t_obs(file_index(i))),'_1.mat'];
    load(file_1_name);
    for e = 1: numel(statesE_new)
        num_parameter = 4;
        plot(U(1,:),[colol(1),'.'])
    end
    
     if i<numel(file_index)
        file_2_name = [workdir,'/states_',num2str(t_obs(file_index(i))),'_2.mat'];
        load(file_2_name);
        for e = 1: numel(statesE_new)
            num_parameter = 4;
            plot(U(1,:),[colol(2),'o'])
        end
             
     end
     
%       if i<numel(file_index)
%         file_3_name = [workdir,'/states_',num2str(t_obs(file_index(i))),'_3.mat'];
%         load(file_3_name);
%         for e = 1: numel(statesE_new)
%             num_parameter = 4;
%             plot(U(1,:),[colol(3),'*'])
%         end
%              
%       end
     xlabel('Ensembles number')
     ylabel('Parameter value')

end
        hold off
    
