function [model_forecast,varargout] = run_mrst_model(p,parameters,problem_in,dt)
          
            problem = control2problem_enfk(p,problem_in, parameters);
            
            model = problem.SimulatorSetup.model;
            state0 = initState(model.G,...
                               problem.SimulatorSetup.schedule.control.W,...
                               problem.SimulatorSetup.state0.pressure,...
                               problem.SimulatorSetup.state0.s); 
                               
            schedule = simpleSchedule(dt, 'W', problem.SimulatorSetup.schedule.control.W);
            
            
            
            
            [wellSols, states] = simulateScheduleAD(state0, model, schedule);
        
            for i = 1: numel(wellSols)
                model_forecast_i = vertcat(wellSols{i}.bhp);
                model_forecast.data(i,1)= model_forecast_i(1); %BHP first well
            end
                model_forecast.time = cumsum(dt);
            if nargout>1
                varargout{1} = states;
            end
end

