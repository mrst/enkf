function model_out = control2model(p,model_in,parameters)

% Initialize the model
model_out =  model_in;



% %Add parameter in to the function model
% if numel(model_in(0)) ==2
%     model_function=@(t,p)[(t-p(1)).*(t-p(2)).*(t-p(3)).*p(4);(t-2*p(1)).*(t-p(2)/5).*(t-p(3)/2).*(-0.5*p(4))+1];
% elseif numel(model_in(0)) ==1
        model_function=@(t,p)[(t-p(1)).*(t-p(2)).*(t-p(3)).*p(4)];
%end

model_out=@(t)model_function(t,p); %This two lines actually 
%creates a new model. Neverthelles, models in MRST will allow to add a 
%particular parameter to a already creted model.


end