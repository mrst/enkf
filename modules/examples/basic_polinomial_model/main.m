

%% Defining a model a experimental data

%Model
p = [2.5,5,7.5,0.01];
model_function=@(t,p)[(t-p(1)).*(t-p(2)).*(t-p(3)).*p(4)];
model=@(t)model_function(t,p);


time =  0:0.05:10;

%Observable data
t_obs = 1:0.5:10;
model_obs = model_function(t_obs,[2,4,8,0.012]);

observation.time = t_obs';
observation.data = model_obs';

% Model vs Observation
plot(time,model(time),t_obs,model_obs,'*')
legend('Model','Observation')


%% Defining paramters

          % Parameters structure is unsues for now but it will becomes 
          % handly to describe parameters caracteristics in MRST models                

          scaling_coeficient = struct('name','coeficient',...
                                   'type','value',...
                                   'boxLims', [0,1],...
                                   'distribution','general',...
                                   'Indx',4);

          polinomial_roots = struct('name','roots',...
                                   'type','value',...
                                   'boxLims', [0 1;0 1;0 1] ,...
                                   'distribution','general',...
                                   'Indx',{1,2,3});                               
                                                                
            parameters =  {}; 
            parameters{1}  = polinomial_roots ;
            parameters{2} = scaling_coeficient;
            

            

%% Defining model class

Model = ModelClass(model,parameters,@control2model,@run_model,'TimeVar','time');

states = Model.runModel([3 4.5 6 0.01],time);


            % Model vs Observation vs  Model Class
            plot(time,model(time),t_obs,model_obs,'*', time,states.data)
            legend('Model','Observation','Model Model')
  %% Ensembles
  %  for j = 1:30
%                rng('shuffle'); a = (rand(1)-0.5)*0.3  +2.5;
%                rng('shuffle'); b = (rand(1)-0.5)*0.3  +5;
%                rng('shuffle'); c = (rand(1)-0.5)*0.5  +7.5;
%                rng('shuffle'); d = (rand(1)-0.5)*0.001 +0.01;
%                 
%                pE{j} = [a b c d];
%  end
%  save('ModelEnsembles','pE');
               
  
  ppE=load('ModelEnsembles.mat');
  Model.pE = ppE.pE;  

  %% Running EnKf            
    mrstEnKF2(Model,observation,...
                         'modelname',   'Polinomial',  ...
                         'method',      'EnKF',      ...
                         'scheme',           1,      ...
                         'nmembers',    numel(Model.pE),...  %Necesary?
                         'restart',     1,           ...
                         'inflation',   0,           ...
                         'transform', {0},          ...
                         'niterations', 2,           ...
                         'Alpha',       1,           ...
                         'nrepeats',    0,           ...
                         'tolerance',   0.01,        ...
                         'tsim',        8,    ...   %Necesary?
                         'tend',        6,    ...   %Necesary?
                         'dt',          0.5,      ...
                         'dtu',         2,    ...
                         'dto',         1,    ...
                         'dts',         1,    ...
                         'transformobs', {0},    ... % transformation functions for the states  % see applyTransform.m for details
                         'itransformobs',{0},  ...
                         'nstat',        [3 1],    ...     ...
                         'obsnum',      numel(observation.time), ...  %Necesary?
                         'Sigma',       0.01, ...
                         'obstype',   { 'flx'},...
                         'wellobs_indx', {{1}});      
            
    plot_enkf            
            