
 input_file = "output.m";
[~ ,name, ~] = fileparts(input_file);
workdir = [mrstOutputDirectory filesep 'ENKF' filesep   convertStringsToChars(name) ];   


file_index = [2 4 6 8];

colol = 'brcm';

plot(t_obs,model_obs,'ko')

    hold on
for i =  1: numel(file_index)
    file_1_name = [workdir,'/summary_',num2str(file_index(i)),'_iter1.mat'];
    load(file_1_name);
    for e = 1: numel(statesE_new)
        plot(timeSteps,statesE_new{e}.data,colol(i))
    end
    
     if i<numel(file_index)
         file_2_name = [workdir,'/summary_',num2str(file_index(i)),'_iter2.mat'];
         load(file_2_name);
             for e = 1: numel(statesE_new)
                 plot(timeSteps,statesE_new{e}.data,['.',colol(i)])
             end
             
     end
     
     if i<numel(file_index)
         file_3_name = [workdir,'/summary_',num2str(file_index(i)),'_iter3.mat'];
         load(file_3_name);
             for e = 1: numel(statesE_new)
                 plot(timeSteps,statesE_new{e}.data,['s',colol(i)])
             end
             
     end
     
end
    hold off


figure
        hold on

ne = numel(Model.pE);        
for i =  1: numel(file_index)
    file_1_name = [workdir,'/states_',num2str(file_index(i)),'_1.mat'];
    load(file_1_name);
    for e = 1: numel(statesE_new)
        num_parameter = 4;
        plot(1:ne,U(1,1:ne),[colol(1),'.'])
        plot(1:ne,U(2,1:ne),[colol(1),'.'])
        plot(1:ne,U(3,1:ne),[colol(1),'.'])
        plot(1:ne,U(4,1:ne),[colol(1),'.'])
    end
    
     if i<numel(file_index)
        file_2_name = [workdir,'/states_',num2str(file_index(i)),'_2.mat'];
        load(file_2_name);
        for e = 1: numel(statesE_new)
            num_parameter = 4;
            plot(1:ne,U(1,1:ne),[colol(2),'o'])
            plot(1:ne,U(2,1:ne),[colol(2),'o'])
            plot(1:ne,U(3,1:ne),[colol(2),'o'])
            plot(1:ne,U(4,1:ne),[colol(2),'o'])
        end
             
     end
     
      if i<numel(file_index)
        file_3_name = [workdir,'/states_',num2str(file_index(i)),'_3.mat'];
        load(file_3_name);
        for e = 1: numel(statesE_new)
            num_parameter = 4;
            plot(1:ne,U(1,1:ne),[colol(3),'*'])
            plot(1:ne,U(2,1:ne),[colol(3),'*'])
            plot(1:ne,U(3,1:ne),[colol(3),'*'])
            plot(1:ne,U(4,1:ne),[colol(3),'*'])
        end
             
     end
end
        hold off

    
