function states = run_model(p,parameters, model, t)

    %Adding parameters value p into the model
         model  = control2model(p,model,parameters);
         
    %Evaluate the model a times t;
         states.data = model(t)';
         states.time = t';
