function ensParams = fixEnsembleParameters(ensParams, parameters)
    %FIXENSEMBLEPARAMETERS Summary of this function goes here
    %   Detailed explanation goes here
    %
    % Similar structure as control2problem_enkf. 

    numParams = numel(parameters);
    Ne = numel(ensParams);
    
    minPorosity = 0.02;
    maxPorosity = 0.5;
    
    
    for i=1:Ne
        reel = 1;
        for k = 1:numParams
            numValues = 1;
            if strcmp(parameters{k}.distribution, 'connection') || ...
                    strcmp(parameters{k}.distribution, 'distribution') || ...
                    strcmp(parameters{k}.distribution, 'well')
                numValues = numel(parameters{k}.Indx);
            elseif ~strcmp(parameters{k}.distribution, 'general')
                warning('Distribution %s is not implemented', parameters{k}.distribution)
            end

            switch parameters{k}.name
                case 'porosity'
                    for j=reel:reel+numValues-1
                        if ensParams{i}(j) < minPorosity
                            ensParams{i}(j) = minPorosity;
                        elseif ensParams{i}(j) > maxPorosity
                            ensParams{i}(j) = maxPorosity;
                        end
                    end
                case 'permeability'
                    for j=reel:reel+numValues-1
                        if ensParams{i}(j) < 0
                            ensParams{i}(j) = 0;
                        end
                    end
                case 'conntrans'
                    for j=reel:reel+numValues-1
                        if ensParams{i}(j) < 0
                            ensParams{i}(j) = 1e-16;
                        end
                    end
                otherwise
                    warning('Parameter %s is not implemented', parameters{k}.name)
            end % case param.name
            reel = reel + numValues;
            
        end % for parameter set j
        
    end % for ensemble member j
    

end

