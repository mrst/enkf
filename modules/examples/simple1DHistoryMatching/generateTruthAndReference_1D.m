function [observations, problem_ref] = generateTruthAndReference_1D(observationFunction)
    %generateTruthAndReference 
    % Generating a 1D model along with observations of a simulation run.
    %   Detailed explanation goes here
    
    disp('start of function generateTruthAndReference')
    rng(1230);
    clf;

    introPlots = true;
    introAnimations = false;

    %% Define true model
    
    % Define grid and geometry
    dx_2d = 205; 
    G = cartGrid([10, 1, 1], [dx_2d, dx_2d/5, dx_2d/5]*meter^3);
    G = computeGeometry(G);

    % Define rock and fluid
    porosity = 0.3; %gaussianField(G.cartDims, [0.2 0.4]); 
    permeability = porosity.^3.*(1e-5)^2./(0.81*72*(1-porosity).^2);

    %permeability = 100*milli*darcy;
    %porosity = 3;
    rock = makeRock(G, permeability(:), porosity(:));

    if introPlots 
        plotCellData(G, rock.perm)
        xlabel('x'); ylabel('y'), zlabel('z');
        colorbar();
        title('permeability');
        view(30,50);
    end
    
    % Two-phase template model
    fluid = initSimpleADIFluid('phases', 'wo', ...
                               'mu',     [1, 5]*centi*poise, ...
                               'rho',    [1000, 700]*kilogram/meter^3, ...
                               'n',      [2, 2], ...
                               'cR',     1e-8/barsa, ...
                               'sMin',   [.2, .2]);

    % Generic black oil model
    gravity off
    model = GenericBlackOilModel(G, rock, fluid);
    model.gas = false;
    model.OutputStateFunctions = {};

    % Add wells
    W = verticalWell([], G, rock, 1, 1, [1], ...
        'Type', 'rate', 'Val', 300*meter^3/day, ...
        'Radius', 0.1, 'Name', 'I1', 'Comp_i', [1, 0], 'sign', 1);

    W = verticalWell(W, G, rock, G.cartDims(1), 1, [1], ...
        'Type', 'bhp', 'Val', 150*barsa, ...
        'Radius', 0.1, 'Name', 'P1', 'Comp_i', [0, 1], 'sign', -1);
    
    % Initialize model
    initPressure = 200*barsa;
    initSaturation = [0, 1];

    state0 = initState(G, W , initPressure, initSaturation);

    if introPlots
        plotWell(G, W, 'color', 'k')
    end

    
    
    ts = ones(15, 1)*30*day;
    schedule = simpleSchedule(ts, 'W', W);

    problem_ref = packSimulationProblem(state0, model, schedule, ...
                                        'simple1DHistoryMatching');

    [ok, status] = simulatePackedProblem(problem_ref);
    [wellSols, states, reports] = getPackedSimulatorOutput(problem_ref);

    if introAnimations
        % Animate water saturation
        figure
        plotGrid(G, 'FaceAlpha', 0, 'EdgeAlpha', 0.1);
        plotWell(G, W);
        view(30, 50);
        pause(1);
        hs = []; % handle for saturation plot, empty initially
        for i = 1:size(ts, 1)
            hs = plotCellData(G, states{i}.s(:,1), states{i}.s(:,1) > 0.1);
            drawnow, pause(0.5);
        end
    elseif introPlots
        plotCellData(G, states{end}.s(:,1), states{end}.s(:,1) > 0.1);
        colorbar();
    end
    
    if introPlots
        plotWellSols(wellSols)
    end

    
    
    %% Make observations
    % Observe oil production at the produced (well 2)
    
    observations = observationFunction(wellSols, states, ts);

end

