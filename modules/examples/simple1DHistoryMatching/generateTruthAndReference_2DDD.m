function [observations, problem_ref, DD_indices] = generateTruthAndReference_2DDD(observationFunction)
    %generateTruthAndReference 
    % Generating a 2D model along with observations of a simulation run.
    %   Detailed explanation goes here
    
    disp('start of function generateTruthAndReference_2DDD')
    rng(1230);
    clf;

    introPlots = true;
    introAnimations = false;

    %% Define true model
    
    % Define grid and geometry
    dx_2d = 275; 
    G = cartGrid([10, 6, 4], [dx_2d, dx_2d/5, dx_2d/5]*meter^3);
    G = computeGeometry(G);

    % Define rock and fluid
    porosity = gaussianField(G.cartDims, [0.2 0.4]); 
    permeability = porosity.^3.*(1e-5)^2./(0.81*72*(1-porosity).^2);

    rock = makeRock(G, permeability(:), porosity(:));

    if introPlots 
        figure
        plotCellData(G, rock.perm)
        xlabel('x'); ylabel('y'), zlabel('z');
        colorbar();
        title('permeability');
        view(30,50);
    end
    
    % Two-phase template model
    fluid = initSimpleADIFluid('phases', 'wo', ...
                               'mu',     [1, 5]*centi*poise, ...
                               'rho',    [1000, 700]*kilogram/meter^3, ...
                               'n',      [2, 2], ...
                               'cR',     1e-8/barsa, ...
                               'sMin',   [.2, .2]);

    % Generic black oil model
    gravity off
    model = GenericBlackOilModel(G, rock, fluid);
    model.gas = false;
    model.OutputStateFunctions = {};


    % Add wells
    W = verticalWell([], G, rock, 3, 3, [1], ...
        'Type', 'rate', 'Val', 300*meter^3/day, ...
        'Radius', 0.1, 'Name', 'I1', 'Comp_i', [1, 0], 'sign', 1);

    W = verticalWell(W, G, rock, 1, G.cartDims(2), [2], ...
        'Type', 'rate', 'Val', 300*meter^3/day, ...
        'Radius', 0.1, 'Name', 'I2', 'Comp_i', [1, 0], 'sign', 1);

    W = verticalWell(W, G, rock, G.cartDims(1), 1, [3], ...
        'Type', 'bhp', 'Val', 150*barsa, ...
        'Radius', 0.1, 'Name', 'P1', 'Comp_i', [0, 1], 'sign', -1);

    W = verticalWell(W, G, rock, G.cartDims(1), G.cartDims(2), [4], ...
        'Type', 'bhp', 'Val', 150*barsa, ...
        'Radius', 0.1, 'Name', 'P2', 'Comp_i', [0, 1], 'sign', -1);

    % Initialize model
    initPressure = 200*barsa;
    initSaturation = [0, 1];

    state0 = initState(G, W , initPressure, initSaturation);

    if introPlots
        plotWell(G, W, 'color', 'k')
    end
    
    ts = ones(15, 1)*30*day;
    schedule = simpleSchedule(ts, 'W', W);

    problem_truth = packSimulationProblem(state0, model, schedule, ...
                                        'simple2DHistoryMatching_truth');

    [ok, status] = simulatePackedProblem(problem_truth);
    [wellSols, states, reports] = getPackedSimulatorOutput(problem_truth);

    if  introAnimations
        % Animate water saturation
        figure
        title('Water saturation');
        plotGrid(G, 'FaceAlpha', 0, 'EdgeAlpha', 0.1);
        plotWell(G, W);
        view(30, 50);
        pause(1);
        hs = []; % handle for saturation plot, empty initially
        for i = 1:size(ts, 1)
            hs = plotCellData(G, states{i}.s(:,1), states{i}.s(:,1) > 0.1);
            drawnow, pause(0.5);
        end
    elseif introPlots
        plotCellData(G, states{end}.s(:,1), states{end}.s(:,1) > 0.1);
        colorbar();
    end
    
    if introPlots
        plotWellSols(wellSols)
    end


    
    %% Make observations
    % Observe oil production at the produced (well 2)
    
    observations = observationFunction(wellSols, states, ts);

    %% Create data-driven graph of well connections
    DD = WellPairNetwork(model, schedule, states, state0, wellSols);
    DD = DD.filter_wps(1*stb/day);

    if introPlots
        figure;
        DD.plotWellPairConnections();
    end

    %% Setting up a data-driven model in problem structure
    DD_poro = 0.2;
    DD_perm = 1000*milli*darcy;
    
    % Compute the total amount of oil in the original reservoir
    DD_totalReservoirVolume = sum(problem_truth.SimulatorSetup.model.operators.pv)/DD_poro;
    DD_numConnections = size(DD.Graph.Edges, 1);
    DD_cellsPerConnections = 10;
    
    DD_L = (DD_totalReservoirVolume*25)^(1/3);
    DD_G = cartGrid([DD_cellsPerConnections, 1, DD_numConnections], ...
                    [DD_L, DD_L/5 DD_L/5]*meter^3);
    DD_G = computeGeometry(DD_G);
    
    DD_fluid =  model.fluid;
    DD_rock = makeRock(DD_G, DD_perm, DD_poro);

    gravity off
    DD_model = GenericBlackOilModel(DD_G, DD_rock, DD_fluid);
    DD_model.gas=false;
    DD_model.OutputStateFunctions = {};
    
    [DD_model, DD_W, DD_indices] = createDDmodel_1(DD_model, DD_cellsPerConnections, ...
                                              DD.Graph, W);


                                          
                                          
    DD_state0 = initState(DD_G, DD_W, initPressure, initSaturation);
    DD_schedule = simpleSchedule(ts, 'W', DD_W);

    problem_ref = packSimulationProblem(DD_state0, DD_model, ...
                DD_schedule, 'simple2DHistoryMatching_DDreference');

    
    [ok, status] = simulatePackedProblem(problem_ref);
    [DD_wellSols, DD_states, DD_reports] = getPackedSimulatorOutput(problem_ref);

    if introAnimations
        % Animate water saturation
        figure;
        title('water saturation in DD model');
        plotGrid(DD_G, 'FaceAlpha', 0, 'EdgeAlpha', 0.1);
        plotWell(DD_G, DD_W);
        view(30, 50);
        pause(1);
        hs = []; % handle for saturation plot, empty initially
        for i = 1:size(ts, 1)
            hs = plotCellData(DD_G, DD_states{i}.s(:,1), DD_states{i}.s(:,1) > 0.1);
            drawnow, pause(0.5);
        end
    elseif introPlots
        figure;
        plotCellData(DD_G, DD_states{end}.s(:,1), DD_states{end}.s(:,1) > 0.1);
        colorbar();
    end
    
    if introPlots
        plotWellSols(DD_wellSols)
    end
    
end

