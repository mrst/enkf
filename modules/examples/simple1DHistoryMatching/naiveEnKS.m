function new_ensParams = naiveEnKS(ensForecast, ensParams, observations, num_observations, R)
    %NAIVEENKF Summary of this function goes here
    %   Detailed explanation goes here
    
    % Deduct some parameters
    num_params = size(ensParams{1}, 2);
    Ne = size(ensParams, 2);
    numObservables = size(ensForecast{1}.data,2); % Corresponds to, e.g., num wells
    totalNumObservations = num_observations*numObservables;
    
    % Build state vector psi = [ observed state ; params ]
    psi = zeros( totalNumObservations + num_params, Ne);
    for i = 1:Ne
        for w=1:numObservables
            psi(1+num_observations*(w-1):num_observations*w, i) = ensForecast{i}.data(1:num_observations, w);
        end
        psi(totalNumObservations+1:end, i) = ensParams{i};
    end
    
    % Structure the observations in a single vector
    y = observations.data(1:num_observations,:);
    y = y(:); % flattening to single column vector
    
    H_op = @(psi) psi(1:totalNumObservations,:);
    HT_op = @(obs) [obs; zeros(num_params, size(obs,2))];

    % Find covariance structures and ens mean
    psi_mean = mean(psi, 2);
    P = zeros(totalNumObservations + num_params);
    for i=1:Ne
        P = P + (psi(:,i) - psi_mean)*(psi(:,i) - psi_mean)';
    end
    P = P/(Ne - 1);

    % Kalman gain
    K = P*HT_op(eye(totalNumObservations))/(H_op(P*HT_op(eye(totalNumObservations))) + R);

    % Ensemble Kalman smoother
    new_ensParams = {};
    for i=1:Ne

        perturbedModelObs = H_op(psi(:,i)) + sqrt(R)*randn(totalNumObservations,1);
        psi_analysis = psi(:,i) + K*(y - perturbedModelObs);

        new_ensParams{i} = psi_analysis(totalNumObservations+1:end)';
    end

end

