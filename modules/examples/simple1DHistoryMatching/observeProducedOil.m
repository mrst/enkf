function observation = observeProducedOil(wellSols, states, timesteps, wellID)
%OBSERVEPRODUCEDOIL Summary of this function goes here
%   Detailed explanation goes here
% Observe oil production at the produced (well 2)
% With multiple wellIDs (w1, wn) and timesteps (t1, tn) we organize
% the observation vector
    
    observation.time = cumsum(timesteps);
    
    numWells = numel(wellID);
    numTimesteps = numel(timesteps);
    
    observation.data = zeros(numTimesteps, numWells);
    for i = 1:numel(timesteps)
        for w = 1:numWells
            observation.data(i,w) = wellSols{i}(wellID(w)).qOs;
        end
    end
    
end

