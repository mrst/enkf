function [ensMeans, ensVars] = parametersMeanAndVar(ensParams)
    %PARAMETERSMEANANDVAR Summary of this function goes here
    %   Detailed explanation goes here
    
    Ne = size(ensParams, 2);
    Np = size(ensParams{1}, 2);
    
    ensMeans = zeros(1, Np);
    ensVars = zeros(1, Np);
    
    % Mean
    for i=1:Ne
        ensMeans = ensMeans + ensParams{i};
    end
    ensMeans = ensMeans./Ne;
    
    % Variance
    for i=1:Ne
        ensVars = ensVars + (ensParams{i} - ensMeans).^2;
    end
    ensVars = ensVars/(Ne - 1);

end

