function plotEnsembleObservations(ensForecast, observations, numObs, plotTitle)
    %PLOTENSEMBLEOBSERVATIONS Summary of this function goes here
    %   Detailed explanation goes here
    figure
    Ne = size(ensForecast, 2);
    numWells = size(ensForecast{1}.data, 2);
    
    for w=1:numWells
        subplot(numWells, 1, w);
        hold on
        for i=1:Ne
            plot(ensForecast{i}.time, ensForecast{i}.data(:,w), 'color', [1 1 1]*0.6)
        end
        plot(observations.time(1:numObs), observations.data(1:numObs,w), 'm*')
        plot(observations.time(numObs:end), observations.data(numObs:end,w), 'b*')
        
        ensMean = ensForecast{1}.data(:,w);
        for i=2:size(ensForecast,2)
            ensMean = ensMean + ensForecast{i}.data(:,w);
        end
        ensMean = ensMean / Ne; 

        plot(observations.time, ensMean, 'r')
        
        if (numWells > 1)
            title(sprintf('Well %i', w));
        end
        %legend('Observation','Model forecast')
    end
    sgtitle(plotTitle)

end

