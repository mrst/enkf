
 input_file = "output.m";
[~ ,name, ~] = fileparts(input_file);
workdir = [mrstOutputDirectory filesep 'ENKF' filesep   convertStringsToChars(name) ];   

iter = 4;
 cmap = colormap(cool(iter));

file_index = 450*day();


for sp = 1:2
subplot(1,2,sp)
        colol = 'brcm';

       % plot(observation.time,observation.data(:,sp),'ko')
          
        for i =  1:iter
            file_1_name = [workdir,'/summary_',num2str(file_index),'_iter',num2str(i),'.mat'];
            load(file_1_name);
            for e = 1: numel(statesE_new)
                hold on                
                h = plot(convertTo(timeSteps,day()),convertTo(-statesE_new{e}.data(:,sp),stb()/day()),'-k');
                cl=cmap(i,:) ;
                set(h,'Linestyle','-','Linewidth',1,'Color',cl,'Marker','none','MarkerSize',2,'MarkerFaceColor',cl,'MarkerEdgeColor',cl);
                hold off        
            end            
           
        end
        hold on
        h_obs = plot(convertTo(timeSteps, day()),convertTo(-observations.data(:,sp),stb()/day()),'k-');
        hold off
        xlabel('Time [day]')
        ylabel('Oil Rate [stb/day]')
        title(['Well ',num2str(sp)])
        
        
       lgd=  legend([h h_obs],{'Last iterations', 'Observations'});
end


