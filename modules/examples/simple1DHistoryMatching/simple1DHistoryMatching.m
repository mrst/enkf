mrstModule add ad-core ad-blackoil mrst-gui ad-props incomp optimization

observationFunction = @(wellSols, states, timesteps) ...
    observeProducedOil(wellSols, states, timesteps, 2);
runModelFunction = @(p, parameters,problem_in,dt) ...
    runModel_simpleEnKF(p, parameters,problem_in,dt, observationFunction);

saveFolder = fullfile(mrstPath('enkf'), 'examples', 'simple1DHistoryMatching', ...
    'simple1D');
if exist(saveFolder) ~= 7
    mkdir(saveFolder)
end

%% Read observations or create truth
[observations, problem_ref] = generateTruthAndReference_1D(observationFunction);


%% Define parameters that are set for this problem
new_perm = 1.2345e-13;
new_poro = 0.2;
perm_struct = struct('name', 'permeability', ...
                     'type', 'value', ...
                     'boxLims', [0, 1], ...
                     'distribution', 'general', ...
                     'Indx', 'not relevant');
poro_struct = struct('name', 'porosity', ...
                     'type', 'value', ...
                     'boxLims', [0, 1], ...
                     'distribution', 'general', ...
                     'Indx', 'not relevant');

parameters =  {}; 
parameters{1} = perm_struct;
parameters{2} = poro_struct;

problem = control2problem_enfk([new_perm, new_poro], problem_ref, parameters);


Model = ModelClass(problem, parameters, ...
                   @control2problem_enfk, runModelFunction, ...
                   'TimeVar', 'dt');

model_forecast = Model.runModel([new_perm/1000 0.2], ...
    observations.time);
%cumsum(problem.SimulatorSetup.schedule.step.val));

% Model vs Observation
figure
t_obs =  cumsum(problem_ref.SimulatorSetup.schedule.step.val);
forecast_time = cumsum(problem_ref.SimulatorSetup.schedule.step.val);
plot(observations.time, observations.data, '*', model_forecast.time, model_forecast.data)
legend('Observation','Model forecast')

%% Define ensemble of parameters
Ne = 20;
ensParams = {};

% Check ensemble members
if true || isfile('ensemble_parameters.mat') == 0
    for i=1:Ne
        ensParams{i} = [exp(-20 + randn()*5), randn()*0.1 + 0.34];
        if ensParams{i}(1) > 0.1
            ensParams{i}(1) = ensParams{i}(1)/1000;
        end
        if ensParams{i}(2) > 1 || ensParams{i}(2) < 0
            ensParams{i}(2) = rand(1);
        end
    end
    save('ensemble_parameters','ensParams')
else
    load('ensemble_parameters', 'ensParams')
end
Model.pE = ensParams;

ensForecast = {};
for i=1:Ne
    ensForecast{i} = Model.runModel(ensParams{i}, observations.time);
end

%% Plot ensemble against observation
plotEnsembleObservations(ensForecast, observations, 'Monte Carlo experiment')
saveas(gcf, fullfile(saveFolder, 'montecarlo.png'));
[mcParamsMean, mcParamsVar] = parametersMeanAndVar(ensParams);

%% Run History Matching with EnKS

num_observations = 8;

% observation error:
%obs_stddev = 0.0001;
%obs_stddev = 0.0000005;
obs_stddev = 0.0000001;

R = eye(num_observations)*obs_stddev;
plot(observations.time, observations.data + rand(size(observations.data))*sqrt(obs_stddev), 'v')


new_ensParams = naiveEnKS(ensForecast, ensParams, observations, num_observations, R);


%% Run ensemble simulation with new parameters

newEnsForecast = {};
for i=1:Ne
    newEnsForecast{i} = Model.runModel(new_ensParams{i}, observations.time);
end

%% Plot new results
plotEnsembleObservations(newEnsForecast, observations, 'History matched experiment (EnKS)')
saveas(gcf, fullfile(saveFolder, 'enks.png'));
[EnKSParamsMean, EnKSParamsVar] = parametersMeanAndVar(new_ensParams);


%input_file = 'inputSettingsSimple1D_twoSteps.m';
%input_file = 'inputSettingsSimple1D_smoother.m';

%naiveEnKS(Model, observations, input_file);

%% ES-MDA

alpha = [9+1/3, 7, 4, 2];

ESMDA_ensForecast = ensForecast;
ESMDA_params = ensParams;
ESMDA_means = {};
ESMDA_vars  = {};

for j=1:size(alpha,2)
    
    ESMDA_params = naiveEnKS(ESMDA_ensForecast, ESMDA_params, ...
        observations, num_observations, R*alpha(j));

    ESMDA_ensForecast = {};
    for i=1:Ne
        fprintf('Running ensemble member %i after ES-MDA iteration %i\n', i, j);
        ESMDA_ensForecast{i} = Model.runModel(ESMDA_params{i}, observations.time);
    end 
    
    plotEnsembleObservations(ESMDA_ensForecast, observations, ...
        sprintf('After iteration %i of ES-MDA',j));
    saveas(gcf, fullfile(saveFolder, sprintf('es-mda-it%i.png',j)));
    [ESMDA_means{j}, ESMDA_vars{j}] = parametersMeanAndVar(ESMDA_params);
end

%% Print results (parameters mean and variance
trueParams = [problem_ref.SimulatorSetup.model.rock.perm(1), ...
               problem_ref.SimulatorSetup.model.rock.poro(1)];

% Permeability and porosity
titles = ['permeability'; 'porosity    '];
for i=1:2
    figure
    hold on
    plot([0,7], [trueParams(i), trueParams(i)]);
    errorbar([1], [mcParamsMean(i)], sqrt([mcParamsVar(i)]), 'v');
    errorbar([2], [EnKSParamsMean(i)], sqrt([EnKSParamsVar(i)]), 'v');
    errorbar([3,4,5,6], [ESMDA_means{1}(i), ESMDA_means{2}(i), ...
                         ESMDA_means{3}(i), ESMDA_means{4}(i)], ...
                        sqrt([ESMDA_vars{1}(i), ESMDA_vars{2}(i), ...
                              ESMDA_vars{3}(i), ESMDA_vars{4}(i)]), 'v');
    legend('truth', 'MC', 'EnKS', 'ES-MDA');
    title(titles(i,:));
end


