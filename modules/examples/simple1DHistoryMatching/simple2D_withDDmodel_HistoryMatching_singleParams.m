mrstModule add ad-core ad-blackoil diagnostics mrst-gui ad-props incomp ...
    optimization ddmodel enkf

mrstVerbose off


observationFunction = @(wellSols, states, timesteps) ...
    observeProducedOil(wellSols, states, timesteps, [3 4]); 
    % [3 4] are well indices for the producers

runModelFunction = @(p, parameters,problem_in, dt) ...
    runModel_simpleEnKF(p, parameters,problem_in, dt, observationFunction);


saveFolder = fullfile(mrstPath('enkf'), 'modules', 'examples', 'simple1DHistoryMatching', ...
    'simple2DtoDD_singleParams');

if exist(saveFolder) ~= 7
    mkdir(saveFolder)
end

num_observations = 8;
Ne = 100;



%% Read observations or create truth
[observations, problem_ref, DD_indices] = generateTruthAndReference_2DDD(observationFunction);

%% Define parameters that are set for this problem
new_perm = [400 800 1200 1600].*milli*darcy;
new_poro = [0.14 0.18 0.22 0.26];
new_wi   = zeros(1,4);
for i=1:4
    new_wi(i) = logNormLayers([1 1 1])*1e-12;
end
perm_struct = struct('name', 'permeability', ...
                     'type', 'value', ...
                     'boxLims', [0, 1], ...
                     'distribution', 'connection', ...
                     'log', false, ...
                     'Indx', {DD_indices.cells});
poro_struct = struct('name', 'porosity', ...
                     'type', 'value', ...
                     'boxLims', [0, 1], ...
                     'distribution', 'connection', ...
                     'Indx', {DD_indices.cells});
wi_struct   = struct('name', 'conntrans', ...
                     'type', 'value', ...
                     'boxLims', [0, 1], ...
                     'distribution', 'well', ...
                     'log', 'false', ...
                     'Indx', [1:4]); % Indx of wells

parameters =  {}; 
parameters{1} = perm_struct;
parameters{2} = poro_struct;
parameters{3} = wi_struct;

paramVector = [new_perm, new_poro, new_wi];

problem = control2problem_enfk(paramVector, problem_ref, parameters);

runSingleAndAnimate = false;
if runSingleAndAnimate
    fullObsFunction = @(wellSols, states, timesteps) {wellSols, states, timesteps};

    extreme_poro = [0.1 0.95 0.05 0.9];
    model_out = runModel_simpleEnKF([new_perm, extreme_poro, new_wi], parameters, ...
        problem, ts, fullObsFunction);
    wellSols = model_out{1};
    states = model_out{2};
    timesteps = model_out{3};


    % Animate water saturation
    figure
    plotGrid(problem.SimulatorSetup.model.G, 'FaceAlpha', 0, 'EdgeAlpha', 0.1);
    plotWell(problem.SimulatorSetup.model.G, ...
        problem.SimulatorSetup.schedule.control.W);
    view(30, 50);
    pause(1);
    hs = []; % handle for saturation plot, empty initially
    for i = 1:size(ts, 1)
        hs = plotCellData(problem.SimulatorSetup.model.G, ...
            states{i}.s(:,1), states{i}.s(:,1) > 0.1);
        drawnow, pause(0.5);
    end
end
    
%% Create model 
Model = ModelClass(problem, parameters, ...
                   @control2problem_enfk, runModelFunction, ...
                   'TimeVar', 'dt');


model_forecast = Model.runModel(paramVector, ...
    observations.time);
%cumsum(problem.SimulatorSetup.schedule.step.val));

numWells = size(observations.data, 2);

% Model vs Observation
figure
for i=1:numWells
    subplot(numWells,1,i);
    %t_obs =  cumsum(problem_ref.SimulatorSetup.schedule.step.val);
    %forecast_time = cumsum(problem_ref.SimulatorSetup.schedule.step.val);
    plot(observations.time, observations.data(:,i), '*', ...
        model_forecast.time, model_forecast.data(:,i))
    legend('Observation','Model forecast')
end

%% Define ensemble of parameters
ensParams = {};

% Check ensemble members
if false || isfile('ensemble_parameters2DtoDD.mat') == 0
    for i=1:100
        ensParams{i} = zeros(size(paramVector));
        ensParams{i}(1:numel(new_perm)) = (randn(size(new_perm))*400 + 1000)*milli*darcy;
        ensParams{i}(numel(new_perm)+1:numel([new_perm, new_poro])) = randn(size(new_poro))*0.13 + 0.2;
        for j=numel([new_perm, new_poro])+1:numel(paramVector)
            ensParams{i}(j) = logNormLayers([1 1 1])*1e-12;
        end
    end
    ensParams = fixEnsembleParameters(ensParams, parameters);
    save('ensemble_parameters','ensParams')
else
    load('ensemble_parameters', 'ensParams')
end
if Ne < 100
    tmpEnsParams = ensParams;
    ensParams = {};
    for i=1:Ne
        ensParams{i}= tmpEnsParams{i};
    end
end

Model.pE = ensParams;

%for i=1:Ne
    %ensParams{i}(3:4) = ensParams{i}(3:4)/1000;
    %ensParams{i}(5:6) = ensParams{i}(5:6)+0.2;
%end

%% Run Monte Carlo experiment
ensForecast = {};
for i=1:Ne
    fprintf('Running MC ensemble member %i\n', i);
    ensForecast{i} = Model.runModel(ensParams{i}, observations.time);
end

%% Plot ensemble against observation
plotEnsembleObservations(ensForecast, observations, num_observations, 'Monte Carlo experiment')
saveas(gcf, fullfile(saveFolder, 'montecarlo.png'));
[mcParamsMean, mcParamsVar] = parametersMeanAndVar(ensParams);

%% Run History Matching with EnKS



% observation error:

%obs_stddev = 0.0001;
%obs_stddev = 0.0000005;

%obs_stddev = 0.0000001; % This value is good!

obs_stddev = eps;



R = eye(num_observations*numWells)*obs_stddev;

for w=1:numWells
    subplot(numWells, 1, w)
    hold on
    plot(observations.time, ...
        observations.data(:,w) + rand(size(observations.data(:,w)))*sqrt(obs_stddev), 'v')
end

enksParams = ensParams;
enksForecast = ensForecast;
for ps = 1:3
    % ps == 3 % permeability
    paramSubsetStart = 1; 
    paramSubsetEnd = 4;
    paramText = "WiPoroPerm";
    if ps == 1 % WI
        paramSubsetStart = 9;
        paramSubsetEnd = 12;
        paramText = "Wi";
    elseif ps == 2 % porosity
        paramSubsetStart = 5;
        paramSubsetEnd = 8;
        paramText = "WiPoro";
    end
    enksParamsSubset = {};
    for i = 1:Ne
        enksParamsSubset{i} = enksParams{i}(paramSubsetStart:paramSubsetEnd);
    end
    enksParamsSubset = naiveEnKS(enksForecast, enksParamsSubset, ...
        observations, num_observations, R);
    for i = 1:Ne
        enksParams{i}(paramSubsetStart:paramSubsetEnd) = enksParamsSubset{i};
    end
    enksParams = fixEnsembleParameters(enksParams, parameters);
    
    % Run new forecast with new parameters
    for i=1:Ne
        fprintf('Running EnKS ensemble member %i after EnKS, paramset %s\n', i, paramText);
        enksForecast{i} = Model.runModel(enksParams{i}, observations.time);
    end
    
    plotEnsembleObservations(enksForecast, observations, num_observations, 'History matched experiment (EnKS)')
    saveas(gcf, fullfile(saveFolder, sprintf('enks_%s.png',paramText)));

end
%new_ensParams = naiveEnKS(ensForecast, ensParams, observations, num_observations, R);
%new_ensParams = fixEnsembleParameters(new_ensParams, parameters);


%% Plot new results
plotEnsembleObservations(enksForecast, observations, num_observations, 'History matched experiment (EnKS)')
saveas(gcf, fullfile(saveFolder, 'enks.png'));
[EnKSParamsMean, EnKSParamsVar] = parametersMeanAndVar(enksParams);


%input_file = 'inputSettingsSimple1D_twoSteps.m';
%input_file = 'inputSettingsSimple1D_smoother.m';

%naiveEnKS(Model, observations, input_file);
%% ES-MDA

alpha = [9+1/3, 7, 4, 2];

ESMDA_ensForecast = ensForecast;
ESMDA_params = ensParams;
ESMDA_means = {};
ESMDA_vars  = {};

for j=1:size(alpha,2)
    
    for ps = 1:3
        % ps == 3 % permeability
        paramSubsetStart = 1; 
        paramSubsetEnd = 4;
        paramText = "WiPoroPerm";
        if ps == 1 % WI
            paramSubsetStart = 9;
            paramSubsetEnd = 12;
            paramText = "Wi";
        elseif ps == 2 % porosity
            paramSubsetStart = 5;
            paramSubsetEnd = 8;
            paramText = "WiPoro";
        end
        esmdaParamsSubset = {};
        for i = 1:Ne
            esmdaParamsSubset{i} = ESMDA_params{i}(paramSubsetStart:paramSubsetEnd);
        end
        esmdaParamsSubset = naiveEnKS(ESMDA_ensForecast, esmdaParamsSubset, ...
            observations, num_observations, R*alpha(j));
    
         for i = 1:Ne
            ESMDA_params{i}(paramSubsetStart:paramSubsetEnd) = esmdaParamsSubset{i};
         end
         ESMDA_params = fixEnsembleParameters(ESMDA_params, parameters);
        


        ESMDA_ensForecast = {};
        for i=1:Ne
            fprintf('Running ensemble member %i after ES-MDA iteration %i, paramset %s\n', i, j, paramText);
            ESMDA_ensForecast{i} = Model.runModel(ESMDA_params{i}, observations.time);
        end 

        plotEnsembleObservations(ESMDA_ensForecast, observations, num_observations, ...
            sprintf('After iteration %i of ES-MDA, paramset %s',j, paramText));
        saveas(gcf, fullfile(saveFolder, sprintf('es-mda-it%i_%s.png',j, paramText)));
    end
    [ESMDA_means{j}, ESMDA_vars{j}] = parametersMeanAndVar(ESMDA_params);
end
    
%% Print results (parameters mean and variance
trueParams = [problem_ref.SimulatorSetup.model.rock.perm(1), ...
               problem_ref.SimulatorSetup.model.rock.poro(1)];

% Permeability and porosity
titles = ['permeability'; ...   
          'porosity    '; ...
          'well-indices'];
offsets = [-0.24 -0.08 0.08 0.24];

for i=1:3
    figure
    startindex = 1+(i-1)*4;
    endindex = i*4;
    
    hold on
    %plot([0,7], [trueParams(i), trueParams(i)]);
    errorbar(offsets + 1, ...
        [mcParamsMean(startindex:endindex)], ...
        sqrt([mcParamsVar(startindex:endindex)]), 'rv');
    
    errorbar(offsets + 2, ...
        [EnKSParamsMean(startindex:endindex)], ...
        sqrt([EnKSParamsVar(startindex:endindex)]), 'bv');
    
    for esmdait=1:4
        errorbar(offsets+esmdait+2, ...
            ESMDA_means{esmdait}(startindex:endindex), ...
            sqrt(ESMDA_vars{esmdait}(startindex:endindex)), 'mv');
    end
    legend('MC', 'EnKS', 'ES-MDA');
    title(titles(i,:));
    saveas(gcf, fullfile(saveFolder, sprintf('%s.png',strip(titles(i,:)))));

end


