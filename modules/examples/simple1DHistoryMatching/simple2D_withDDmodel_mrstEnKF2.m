mrstModule add ad-core ad-blackoil diagnostics mrst-gui ad-props incomp optimization ddmodel

observationFunction = @(wellSols, states, timesteps) ...
    observeProducedOil(wellSols, states, timesteps, [3 4]);
runModelFunction = @(p, parameters,problem_in, dt) ...
    runModel_simpleEnKF(p, parameters,problem_in, dt, observationFunction);

runUsingDDmodelAsTruth = false;
WIParameter = true;
saveFolder = fullfile(mrstPath('enkf'), 'examples', 'simple1DHistoryMatching', ...
    'simple2DtoDD');
if runUsingDDmodelAsTruth
    saveFolder = fullfile(mrstPath('enkf'), 'examples', 'simple1DHistoryMatching', ...
    'simpleDD');
end
if WIParameter
   saveFolder = fullfile(mrstPath('enkf'), 'examples', 'simple1DHistoryMatching', ...
    'simple2DtoDDwithWI');
end
if exist(saveFolder) ~= 7
    mkdir(saveFolder)
end

num_observations = 8;



%% Read observations or create truth
[observations, problem_ref, DD_indices] = generateTruthAndReference_2DDD(observationFunction);

%% Define parameters that are set for this problem
new_perm = [400 800 1200 1600].*milli*darcy;
new_poro = [0.14 0.18 0.22 0.26];
new_wi   = zeros(1,4);
for i=1:4
    new_wi(i) = logNormLayers([1 1 1])*1e-12;
end
perm_struct = struct('name', 'permeability', ...
                     'type', 'value', ...
                     'boxLims', [0, 1], ...
                     'distribution', 'connection', ...
                     'log', false, ...
                     'Indx', {DD_indices.cells});
poro_struct = struct('name', 'porosity', ...
                     'type', 'value', ...
                     'boxLims', [0, 1], ...
                     'distribution', 'connection', ...
                     'Indx', {DD_indices.cells});
wi_struct   = struct('name', 'conntrans', ...
                     'type', 'value', ...
                     'boxLims', [0, 1], ...
                     'distribution', 'well', ...
                     'log', 'false', ...
                     'Indx', [1:4]); % Indx of wells

parameters =  {}; 
parameters{1} = perm_struct;
parameters{2} = poro_struct;
parameters{3} = wi_struct;

paramVector = [new_perm, new_poro, new_wi];

problem = control2problem_enfk(paramVector, problem_ref, parameters);

    
%% Create model 
Model = ModelClass(problem, parameters, ...
                   @control2problem_enfk, runModelFunction, ...
                   'TimeVar', 'dt');


model_forecast = Model.runModel(paramVector, ...
    observations.time);
%cumsum(problem.SimulatorSetup.schedule.step.val));

if runUsingDDmodelAsTruth
    observations = model_forecast;
end

numWells = size(observations.data, 2);

% Model vs Observation
figure
for i=1:numWells
    subplot(numWells,1,i);
    %t_obs =  cumsum(problem_ref.SimulatorSetup.schedule.step.val);
    %forecast_time = cumsum(problem_ref.SimulatorSetup.schedule.step.val);
    plot(observations.time, observations.data(:,i), '*', ...
        model_forecast.time, model_forecast.data(:,i))
    legend('Observation','Model forecast')
end

%% Define ensemble of parameters
Ne = 20;
ensParams = {};

% Check ensemble members
if false || isfile('ensemble_parameters2DtoDD.mat') == 0
    for i=1:100
        ensParams{i} = zeros(size(paramVector));
        ensParams{i}(1:numel(new_perm)) = (randn(size(new_perm))*400 + 1000)*milli*darcy;
        ensParams{i}(numel(new_perm)+1:numel([new_perm, new_poro])) = randn(size(new_poro))*0.13 + 0.2;
        for j=numel([new_perm, new_poro])+1:numel(paramVector)
            ensParams{i}(j) = logNormLayers([1 1 1])*1e-12;
        end
    end
    ensParams = fixEnsembleParameters(ensParams, parameters);
    save('ensemble_parameters','ensParams')
else
    load('ensemble_parameters', 'ensParams')
end
if Ne < 100
    tmpEnsParams = ensParams;
    ensParams = {};
    for i=1:Ne
        ensParams{i}= tmpEnsParams{i};
    end
end

ensParams{6}(1)
%TODO: For some reason this implementaation give problems with zero permeability values :( 
ensParams{6}(1)=733.3570e-015; %
Model.pE = ensParams;



%  %% Running EnKf            
    mrstEnKF2(Model,observations,...
                         'modelname',   'Simple2D_Model',  ...
                         'method',      'EnKF',      ...
                         'scheme',           1,      ...
                         'nmembers',    numel(Model.pE),...  %Necesary?
                         'restart',     1,           ...
                         'inflation',   0,           ...
                         'transform', {0},           ...
                         'niterations', 8,           ...
                         'Alpha',       1,           ...
                         'nrepeats',    0,           ...
                         'tolerance',   0.01,        ...
                         'tsim',       450*day(),    ...   %Necesary?
                         'tend',       450*day(),    ...   %Necesary?
                         'dt',         30*day(),     ...
                         'dtu',        450*day(),    ...
                         'dts',        60*day(),     ...
                         'dto',        60*day(),     ...
                         'transformobs', {1 0 4},    ... % transformation functions for the states  % see applyTransform.m for details
                         'itransformobs',{-1 0 -4},  ...
                         'nstat',        [4 4 4],    ...
                         'obsnum',      numel(observations.time), ...  %Necesary?
                         'Sigma',       2.5 * meter^3/day, ...
                         'obstype',     {'flx'},...
                         'wellobs_indx', {{1:2}});  
plot_enkf                     




